package com.lihan.pethome.pet.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.pet.entity.Pet;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PetMapper extends BaseMapper<Pet> {

    void updateStateonSale(List<Pet> pets);

    void updateStateoffSale(List<Pet> pets);
}