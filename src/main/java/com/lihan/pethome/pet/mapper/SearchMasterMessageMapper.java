package com.lihan.pethome.pet.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.pet.entity.SearchMasterMessage;

public interface SearchMasterMessageMapper extends BaseMapper<SearchMasterMessage> {
}