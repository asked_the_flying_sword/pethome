package com.lihan.pethome.pet.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.pet.entity.PetDetail;

/**
 * @Title: PetDetailMapper
 * @Package:com.lihan.pethome.pet.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/24
 * @version:V1.0
 */
public interface PetDetailMapper extends BaseMapper<PetDetail> {
}
