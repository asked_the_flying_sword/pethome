package com.lihan.pethome.pet.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.GetUserUtil;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.order.entity.Product;
import com.lihan.pethome.order.query.ProductQuery;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.org.service.IShopService;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.pet.query.PetQuery;
import com.lihan.pethome.pet.service.IPetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Title: PetController
 * @Package:com.lihan.pethome.pet.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/24 10:03
 * @version:V1.0
 */
@RestController
@RequestMapping("/pet")
public class PetController {

    @Autowired
    private IPetService petService;

    @Autowired
    private IShopService shopService;

    /**
     * @Description:(作用) 查询当前店铺的所有宠物信息
     * @param: [query, request]
     * @return: com.lihan.pethome.basic.util.PageList<com.lihan.pethome.pet.entity.Pet>
     * @author: lihan
     * @date: 2020/8/24
     * @version:V1.0
     */
    @PatchMapping("/getAll")
    public PageList<Pet> getAll(@RequestBody PetQuery query, HttpServletRequest request) {
        try {
            //获取登录员工对象
            Employee emp = GetUserUtil.getEmp(request);
            //根据员工对象查询店铺
            Shop shop = shopService.findShopState(emp.getId());
            //封装店铺id
            query.setShopId(shop.getId());
            return petService.findAll(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PageList<>();
    }
    @PatchMapping("/onSale")
    public AjaxResult onSale(@RequestBody List<Pet> pets) {
        try {
            //调用service的根据id修改方法
            petService.onSale(pets);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    @PatchMapping("/offSale")
    public AjaxResult offSale(@RequestBody List<Pet> pets) {
        try {
            //调用service的根据id修改方法
            petService.offSale(pets);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * @Description:(作用) 根据id删除宠物
     * @param: [id]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/24
     * @version:V1.0
     */
    @PatchMapping("/del")
    public AjaxResult del(@RequestBody List<Long> ids){
        System.err.println(ids);
        try {
            //调用service的根据id删除方法
            petService.del(ids);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("删除失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 添加商品
     * @param: [pet]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PutMapping
    public AjaxResult save(@RequestBody Pet pet,HttpServletRequest request){
        try {
            //获取登录员工对象
            Employee emp = GetUserUtil.getEmp(request);
            //根据员工对象查询店铺
            Shop shop = shopService.findShopState(emp.getId());
            //封装店铺id
            pet.setShop(shop);
            //调用service的添加方法
            petService.save(pet);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("添加失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 修改商品
     * @param: [pet]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PostMapping
    public AjaxResult update(@RequestBody Pet pet){
        try {
            //调用service的修改方法
            petService.update(pet);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("修改失败：" + e.getMessage());
        }
    }

    //========================================前台的请求=======================================
    /**
     * @Description:(作用) 查询所有上架的宠物信息
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.Product>
     * @author: lihan
     * @date: 2020/8/8
     * @version:V1.0
     */
    @PatchMapping("/loadAll")
    public PageList<Pet> loadAll(@RequestBody PetQuery query){
        return petService.findAll(query);
    }

    /**
     * @Description:(作用) 查询单个宠物
     * @param: [id]
     * @return: com.lihan.pethome.order.entity.Product
     * @author: lihan
     * @date: 2020/8/19
     * @version:V1.0
     */
    @PostMapping("/findById/{id}")
    public Pet toProductInfo(@PathVariable Long id) {
        return petService.findById(id);
    }
}
