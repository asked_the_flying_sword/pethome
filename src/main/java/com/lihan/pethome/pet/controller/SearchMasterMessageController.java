package com.lihan.pethome.pet.controller;

import com.lihan.pethome.basic.entity.Point;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.DistanceUtil;
import com.lihan.pethome.basic.util.GetUserUtil;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.org.service.IShopService;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.pet.entity.SearchMasterMessage;
import com.lihan.pethome.pet.query.SearchMasterMessageQuery;
import com.lihan.pethome.pet.service.ISearchMasterMessageService;
import com.lihan.pethome.user.entity.User;
import com.lihan.pethome.user.mapper.UserMapper;
import com.lihan.pethome.user.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @Title: SearchMasterMessageController
 * @Package:com.lihan.pethome.pet.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/23 13:21
 * @version:V1.0
 */
@RestController
@RequestMapping("/searchMaster")
public class SearchMasterMessageController {

    @Autowired
    private ISearchMasterMessageService searchMasterMessageService;

    @Autowired
    private IShopService shopService;

    @Autowired
    private UserMapper userMapper;
    /**
     * @Description:(作用) 发布寻主信息
     * @param: [searchMasterMessage, request]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    @PutMapping
    public AjaxResult saveSearchMasterMessage(@RequestBody SearchMasterMessage searchMasterMessage, HttpServletRequest request) {
        try {
            //得到用户当前登录的信息
            User user = GetUserUtil.getUser(request);
            User dbUser = null;
            if (Objects.nonNull(user)) {
                if(user.getUsername() != null){
                    dbUser = userMapper.findUserByTel(user.getUsername());
                } else if (user.getEmail() != null) {
                    dbUser = userMapper.findUserByTel(user.getEmail());
                } else if (user.getPhone() != null) {
                    dbUser = userMapper.findUserByTel(user.getPhone());
                }
                //得到user的id存入寻主信息对象中
                searchMasterMessage.setUser(dbUser);
                //得到searchMasterMessage对象的地址，将地址转成坐标
                Point point = DistanceUtil.getPoint(searchMasterMessage.getAddress());
                //得到分配最近的门店
                Shop shop = DistanceUtil.getNearestShop(point, shopService.list());
                searchMasterMessage.setShop(shop);
                searchMasterMessageService.save(searchMasterMessage);
            }
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * @Description:(作用) 根据条件查询
     * @param: [query]
     * @return: com.lihan.pethome.basic.util.PageList<com.lihan.pethome.pet.entity.SearchMasterMessage>
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    @PatchMapping
    public PageList<SearchMasterMessage> loadAllSearchMasterMessage(@RequestBody SearchMasterMessageQuery query,HttpServletRequest request){
        //获取当前登录的员工信息
        Employee emp = GetUserUtil.getEmp(request);
        if (emp != null) {
            //如果emp对象不等于null，就查询关联的店铺
            Shop shop = shopService.findShopState(emp.getId());
            query.setShopId(shop.getId());
            return searchMasterMessageService.findAll(query);
        }
        return new PageList<>();
    }
    /**
     * @Description:(作用) 处理未处理的寻主信息
     * @param: [pet, request]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    @PostMapping("/handlePendingMessage")
    public AjaxResult handlePendingMessage(@RequestBody Pet pet,HttpServletRequest request) {
        try {
            //获取当前登录的员工信息
            Employee emp = GetUserUtil.getEmp(request);
            if (emp != null) {
                //根据登录员工查询对应的店铺信息
                Shop shop = shopService.findShopState(emp.getId());
                //将员工信息保存到店铺对象中
                shop.setAdmin(emp);
                //将店铺信息存入pet对象中
                pet.setShop(shop);
                searchMasterMessageService.handlePendingMessage(pet);
            }
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

}
