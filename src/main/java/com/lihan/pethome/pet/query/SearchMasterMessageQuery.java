package com.lihan.pethome.pet.query;

import com.lihan.pethome.basic.query.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Title: SearchMasterMessageQuery
 * @Package:com.lihan.pethome.pet
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/23 21:12
 * @version:V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SearchMasterMessageQuery extends BaseQuery {

    private String name;
    private Integer state;
    private Long shopId;

}
