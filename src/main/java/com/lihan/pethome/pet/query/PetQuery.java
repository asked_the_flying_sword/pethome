package com.lihan.pethome.pet.query;

import com.lihan.pethome.basic.query.BaseQuery;
import lombok.Data;

/**
 * @Title: PetQuery
 * @Package:com.lihan.pethome.pet.query
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/24 11:43
 * @version:V1.0
 */
@Data
public class PetQuery extends BaseQuery {

    private String name;
    private Integer state;
    private Long shopId;
}
