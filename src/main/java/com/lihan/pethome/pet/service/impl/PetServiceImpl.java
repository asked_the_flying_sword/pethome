package com.lihan.pethome.pet.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.pet.entity.PetDetail;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.pet.entity.SearchMasterMessage;
import com.lihan.pethome.pet.mapper.PetDetailMapper;
import com.lihan.pethome.pet.mapper.PetMapper;
import com.lihan.pethome.pet.service.IPetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class PetServiceImpl extends BaseServiceImpl<Pet> implements IPetService {

    @Autowired
    private PetMapper petMapper;

    @Autowired
    private PetDetailMapper petDetailMapper;

    /**
     * @Description:(作用) 上架宠物
     * @param: [pets]
     * @return: void
     * @author: lihan
     * @date: 2020/8/24
     * @version:V1.0
     */
    @Override
    public void onSale(List<Pet> pets) {
        petMapper.updateStateonSale(pets);
    }
    /**
     * @Description:(作用) 下架宠物
     * @param: [pets]
     * @return: void
     * @author: lihan
     * @date: 2020/8/24
     * @version:V1.0
     */
    @Override
    public void offSale(List<Pet> pets) {
        petMapper.updateStateoffSale(pets);
    }

    /**
     * @Description:(作用) 根据数据id删除
     * @param: [id] 数据id
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void del(List<Long> ids) {
        //先删除petDetailMapper中关联的数据
        petDetailMapper.delete(ids);
        //再删除product信息
        petMapper.delete(ids);
    }

    /**
     * @Description:(作用) 添加数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void save(Pet pet) {
        SearchMasterMessage searchMasterMessage = pet.getSearchMasterMessage();
        if (Objects.isNull(searchMasterMessage)) {
            SearchMasterMessage smm = new SearchMasterMessage();
            smm.setId(0L);
            pet.setSearchMasterMessage(smm);
        }
        //先保存产品信息得到产品的id
        petMapper.save(pet);
        //得到产品详细信息的对象
        PetDetail petDetail = pet.getPetDetail();
        //将得到的product的id给 productDetail对象
        petDetail.setPetId(pet.getId());
        //保存数据
        petDetailMapper.save(petDetail);
    }
    /**
     * @Description:(作用) 修改数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void update(Pet pet) {
        //获得商品详细信息的对象
        PetDetail petDetail = pet.getPetDetail();
        petDetail.setPetId(pet.getId());
        //修改商品的详细信息
        petDetailMapper.update(petDetail);
        //修改商品信息
        petMapper.update(pet);
    }
}
