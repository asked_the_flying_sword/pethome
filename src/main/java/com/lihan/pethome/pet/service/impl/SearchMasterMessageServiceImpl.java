package com.lihan.pethome.pet.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.basic.util.CodeGenerateUtils;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.order.entity.PetAcquisitionOrder;
import com.lihan.pethome.order.mapper.PetAcquisitionOrderMapper;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.pet.entity.PetDetail;
import com.lihan.pethome.pet.entity.SearchMasterMessage;
import com.lihan.pethome.pet.mapper.PetDetailMapper;
import com.lihan.pethome.pet.mapper.PetMapper;
import com.lihan.pethome.pet.query.SearchMasterMessageQuery;
import com.lihan.pethome.pet.service.IPetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.lihan.pethome.pet.mapper.SearchMasterMessageMapper;
import com.lihan.pethome.pet.service.ISearchMasterMessageService;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class SearchMasterMessageServiceImpl extends BaseServiceImpl<SearchMasterMessage> implements ISearchMasterMessageService {
    //寻主信息mapper
    @Resource
    private SearchMasterMessageMapper searchMasterMessageMapper;
    //宠物mapper
    @Autowired
    private PetMapper petMapper;

    //宠物详情mapper
    @Autowired
    private PetDetailMapper petDetailMapper;

    //宠物收购订单mapper
    @Autowired
    private PetAcquisitionOrderMapper petAcquisitionOrderMapper;

    /**
     * @Description:(作用) 保存寻主信息
     * @param: [searchMasterMessage]
     * @return: void
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    @Override
    @Transactional
    public void save(SearchMasterMessage searchMasterMessage) {
        searchMasterMessageMapper.save(searchMasterMessage);
    }
    /**
     * @Description:(作用) 根据条件查询
     * @param: [query]
     * @return: com.lihan.pethome.basic.util.PageList<com.lihan.pethome.pet.entity.SearchMasterMessage>
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    @Override
    public PageList<SearchMasterMessage> findAll(SearchMasterMessageQuery query) {
        return super.findAll(query);
    }
    /**
     * @Description:(作用)
     * @param: [pet] 处理未处理的寻主信息
     * @return: void
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    @Override
    @Transactional
    public void handlePendingMessage(Pet pet) {
        //1.将寻主信息改为已处理
        Long id = pet.getSearchMasterMessage().getId();
        //查询寻主信息
        SearchMasterMessage message = searchMasterMessageMapper.findById(id);
        //得到寻主信息对象
        SearchMasterMessage searchMasterMessage = pet.getSearchMasterMessage();
        //设置为已处理状态
        searchMasterMessage.setState(1);
        //保存到数据库
        searchMasterMessageMapper.update(searchMasterMessage);
        //2.保存宠物
        petMapper.save(pet);
        //3.保存宠物详情
        PetDetail petDetail = pet.getPetDetail();
        petDetail.setPetId(pet.getId());
        petDetailMapper.save(petDetail);

        //创建收购订单
        PetAcquisitionOrder petAcquisitionOrder = createPetAcquisitionOrder(pet,message);
        //保存寻主的收购订单
        petAcquisitionOrderMapper.save(petAcquisitionOrder);
    }
    /**
     * @Description:(作用) 创建宠物收购订单
     * @param: [pet]
     * @return: com.lihan.pethome.order.entity.PetAcquisitionOrder
     * @author: lihan
     * @date: 2020/8/25
     * @version:V1.0
     */
    private PetAcquisitionOrder createPetAcquisitionOrder(Pet pet,SearchMasterMessage searchMasterMessage) {
        PetAcquisitionOrder petAcquisitionOrder = new PetAcquisitionOrder();
        //订单标题
        petAcquisitionOrder.setDigest("[账单]对"+pet.getName()+"收购的订单");
        //0 待支付(余额支付) 1 待报账(垫付) 2 待打款(银行转账)  3 完成
        if (pet.getCostprice().equals(0)) {
            //如果宠物的成本价等于0，订单状态就为完成
            petAcquisitionOrder.setState(3);
        }else{
            //宠物成本价不等于0，状态为1
            petAcquisitionOrder.setState(1);
        }
        //设置收购价格
        petAcquisitionOrder.setPrice(pet.getCostprice());
        //设置收购地址
        petAcquisitionOrder.setAddress(searchMasterMessage.getAddress());
        //设置订单编号  根据寻主信息对象中的用户id 生成订单编号
        petAcquisitionOrder.setOrderSn(CodeGenerateUtils.generateOrderSn(searchMasterMessage.getUser().getId()));
        //设置宠物
        petAcquisitionOrder.setPet(pet);
        //设置信息发布者
        petAcquisitionOrder.setUser(searchMasterMessage.getUser());
        //设置支付类型  0垫付 1余额 2银行转账
        petAcquisitionOrder.setPaytype(0);
        //设置收购订单对应的店铺
        petAcquisitionOrder.setShop(pet.getShop());
        //设置员工
        petAcquisitionOrder.setEmployee(pet.getShop().getAdmin());
        return petAcquisitionOrder;
    }
}
