package com.lihan.pethome.pet.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.pet.entity.SearchMasterMessage;
import com.lihan.pethome.pet.query.SearchMasterMessageQuery;

public interface ISearchMasterMessageService extends IBaseService<SearchMasterMessage> {

    PageList<SearchMasterMessage> findAll(SearchMasterMessageQuery query);

    void handlePendingMessage(Pet pet);
}
