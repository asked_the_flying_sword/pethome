package com.lihan.pethome.pet.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.pet.entity.SearchMasterMessage;
import com.lihan.pethome.pet.query.SearchMasterMessageQuery;

import java.util.List;

public interface IPetService extends IBaseService<Pet> {

    /**
     * @Description:(作用) 上架宠物信息
     * @param: [pets]
     * @return: void
     * @author: lihan
     * @date: 2020/8/24
     * @version:V1.0
     */
    void onSale(List<Pet> pets);

    /**
     * @Description:(作用) 下架宠物信息
     * @param: [pets]
     * @return: void
     * @author: lihan
     * @date: 2020/8/24
     * @version:V1.0
     */
    void offSale(List<Pet> pets);
}
