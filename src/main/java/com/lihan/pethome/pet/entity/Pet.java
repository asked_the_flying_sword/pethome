package com.lihan.pethome.pet.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.user.entity.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Pet {
    private Long id;
    //宠物名（前端传递）
    private String name;
    //宠物资源（前端传递）
    private String resources;
    //销售价格（前端传递）
    private BigDecimal saleprice;
    //成本价格（前端传递）
    private BigDecimal costprice;
    //宠物详情(前端传递过来)
    private PetDetail petDetail = new PetDetail();
    //寻主信息对象(前端传递)
    private SearchMasterMessage searchMasterMessage;
    //宠物类型(前端传递)
    private PetType type;
    //下架时间（后台自动生成）
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss",timezone = "GTM+8")
    private Date offsaletime = new Date();
    //上架时间（后台生成）
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss",timezone = "GTM+8")
    private Date onsaletime;
    //上架状态  0下架   1上架
    private Integer state = 0;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss",timezone = "GTM+8")
    private Date createtime = new Date();
    //店铺(根据登录用户查询出来)
    private Shop shop;
    //记录宠物是被谁领养了的(暂时不管)
    private User user;

}
