package com.lihan.pethome.pet.entity;

import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.user.entity.User;
import lombok.Data;

@Data
public class SearchMasterMessage {
    private Long id;
    private String name;
    private Double price;
    private String age;
    private Integer gender;
    private String coatColor;
    private String resources;
    private PetType petType;
    private String address;
    private Integer state = 0;
    private String title;
    private User user;
    private Shop shop;
}