package com.lihan.pethome.pet.entity;

import lombok.Data;

@Data
public class PetType {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private String description;

    /**
     *
     */
    private Long pid;
}

