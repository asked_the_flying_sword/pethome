package com.lihan.pethome.pet.entity;

import lombok.Data;

/**
 * 宠物详情
 */
@Data
public class PetDetail {
    private Long id;
    //宠物
    private Long petId;
    //宠物领养须知
    private String adoptNotice;
    //宠物简介
    private String intro;
}
