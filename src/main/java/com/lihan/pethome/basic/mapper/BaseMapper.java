package com.lihan.pethome.basic.mapper;

import com.lihan.pethome.basic.query.BaseQuery;

import java.util.List;

/**
 * @Title: BaseMapper
 * @Package:com.lihan.pethome.org.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/10
 * @version:V1.0
 */
public interface BaseMapper<T> {

    /**
     * @Description:(作用) 添加数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    void save(T t);
    /**
     * @Description:(作用) 修改数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    void update(T t);
    /**
     * @Description:(作用) 根据数据id删除
     * @param: [id] 数据id
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    void delete(List<Long> id);
    /**
     * @Description:(作用) 根据id查询
     * @param: [id]
     * @return: com.lihan.pethome.org.entity.T
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    T findById(Long id);

    /**
     * @Description:(作用) 分页高级查询全部
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.T>
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    List<T> findAll(BaseQuery query);
    /**
     * @Description:(作用) 分页高级查询总数
     * @param: [query]
     * @return: java.lang.Integer
     * @author: lihan
     * @date: 2020/8/10
     * @version:V1.0
     */
    Integer findCount(BaseQuery query);

    /**
     * @Description:(作用)    查询全部
     * @param: []
     * @return: java.util.List<T>
     * @author: lihan
     * @date: 2020/8/10
     * @version:V1.0
     */
    List<T> list();

}
