package com.lihan.pethome.basic.config;

import com.lihan.pethome.org.interceptor.EmpLoginInterceptor;
import com.lihan.pethome.user.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Title: WebMvcConfig
 * @Package:com.lihan.pethome.basic.config
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/16 17:44
 * @version:V1.0
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    //前端用户登录拦截器
    @Autowired
    private LoginInterceptor loginInterceptor;

    //员工登录拦截器
    @Autowired
    private EmpLoginInterceptor empLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        //配置登录拦截器
//        registry.addInterceptor(loginInterceptor)
//                .addPathPatterns("/**")
//                .excludePathPatterns("/user/*","/verificationCode/sendPhoneCode")
//                .excludePathPatterns("/wechat/toLogin");
//
//        registry.addInterceptor(empLoginInterceptor)
//                .addPathPatterns("/**")
//                .excludePathPatterns("/login","/shop/settledIn");
    }
}
