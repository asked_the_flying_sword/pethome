package com.lihan.pethome.basic.service.impl;

import com.lihan.pethome.basic.service.ISmsService;
import com.lihan.pethome.basic.util.*;
import com.lihan.pethome.user.constant.PhoneConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.lihan.pethome.user.constant.PhoneConstant.PHONE_REG;

/**
 * @Title: SmsServiceImpl
 * @Package:com.lihan.pethome.basic.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14 19:02
 * @version:V1.0
 */
@Service
public class SmsServiceImpl implements ISmsService {

    
    /**
     * @Description:(作用) 注册发送短信
     * @param: [phone] 手机号
     * @return: com.lihan.pethome.basic.util.AjaxResult  
     * @author: lihan
     * @date: 2020/8/14
     * @version:V1.0
     */
    @Override
    public AjaxResult sendPhoneRegCode(String phone) {
        //获取redis的key值
        return sendCode(phone,PhoneConstant.PHONE_REG);
    }
    /**
     * @Description:(作用) 登录发送短信
     * @param: [phone] 手机号
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/14
     * @version:V1.0
     */
    @Override
    public AjaxResult sendPhoneLogCode(String phone) {
        return sendCode(phone,PhoneConstant.PHONE_LOG);
    }

    /**
     * @Description:(作用) 发短信同意方法
     * @param: [phone, prefix] 手机号，前缀
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    private AjaxResult sendCode(String phone,String prefix) {
        //获取redis的key值
        String key = prefix + ":" + phone;
        //生成短信验证码
        String code = StrUtils.getComplexRandomString(4);
        //取出redis中验证码
        String value = RedisUtils.INSTANCE.get(key);
        //判断字符串是否为null，不为null，就是第二次发送验证码
        if (StringUtils.isNotBlank(value)) {
            //获取第一次生成验证码的时间戳
            String oldTime = value.split(":")[1];
            //获取当前时间
            Long newTime = System.currentTimeMillis();
            //当前时间-第一次验证码的时间  得到时间差 ,判断时间差是否小于一分钟
            if (newTime - Long.valueOf(oldTime) < 60 * 1000) {
                return AjaxResult.error("请不用在一分钟内，发送多次");
            }
            //间隔时间大于一分钟，小于5分钟，就使用上一个验证码
            code = value.split(":")[0];
        }
        //存入redis的value值
        String val = code + ":" + System.currentTimeMillis();
        //将用户的手机号、验证码、验证码的有效期存入redis
        RedisUtils.INSTANCE.set(key, val, 60 * 5);
        System.out.println("Hello 你好，这是你的验证码：" + code + " ,有效期为5分钟");
        String text = "Hello 你好，这是你的验证码：" + code + " ,有效期为5分钟";
        //阿里云短信发送
//        SendSmsUtil.sendSms(phone, code);
        //网建短信
//        SmsUtil.sendPhoneCode(phone,text);
        return AjaxResult.ok();
    }
}
