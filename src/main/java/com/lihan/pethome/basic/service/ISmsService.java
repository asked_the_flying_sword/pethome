package com.lihan.pethome.basic.service;

import com.lihan.pethome.basic.util.AjaxResult;

/**
 * @Title: ISmsService
 * @Package:com.lihan.pethome.basic.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14
 * @version:V1.0
 */
public interface ISmsService {
    //注册发送短信
    AjaxResult sendPhoneRegCode(String phone);

    //登录发送短信
    AjaxResult sendPhoneLogCode(String phone);
}
