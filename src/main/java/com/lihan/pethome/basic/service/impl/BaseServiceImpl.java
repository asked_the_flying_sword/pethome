package com.lihan.pethome.basic.service.impl;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.basic.query.BaseQuery;
import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Title: BaseServiceImpl
 * @Package:com.lihan.pethome.org.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/10 10:11
 * @version:V1.0
 */
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class BaseServiceImpl<T> implements IBaseService<T>{

    @Autowired
    private BaseMapper<T> baseMapper;

    /**
     * @Description:(作用) 添加数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void save(T t) {
        baseMapper.save(t);
    }
    /**
     * @Description:(作用) 修改数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void update(T t) {
        baseMapper.update(t);
    }
    /**
     * @Description:(作用) 根据数据id删除
     * @param: [id] 数据id
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void del(List<Long> ids) {
        baseMapper.delete(ids);
    }
    /**
     * @Description:(作用) 根据id查询
     * @param: [id]
     * @return: com.lihan.pethome.org.entity.T
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    public T findById(Long id) {
        return baseMapper.findById(id);
    }
    /**
     * @Description:(作用) 分页高级查询全部
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.T>
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    public PageList<T> findAll(BaseQuery query) {
        //根据条件查询总条数
        Integer total = baseMapper.findCount(query);
        if(total.equals(0)){
            //如果总条数为0，返回null对象
            return new PageList<>();
        }
        //根据条件查询
        List<T> ts = baseMapper.findAll(query);
        return new PageList<>(ts,total);
    }
    /**
     * @Description:(作用) 查询全部
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.T>
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    public List<T> list() {
        return baseMapper.list();
    }

}
