package com.lihan.pethome.basic.query;

import lombok.Data;

/**
 * @Title: BaseQuery
 * @Package:com.lihan.pethome.demo.query
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/7 16:04
 * @version:V1.0
 */
@Data
public class BaseQuery {

    private Integer currentPage = 1;
    private Integer pageSize = 10;

    public Integer getBegin(){
        return (this.currentPage - 1)*this.pageSize;
    }

}
