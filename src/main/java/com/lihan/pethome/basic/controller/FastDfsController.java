package com.lihan.pethome.basic.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.FastDfsApiOpr;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @Title: FastDfsController
 * @Package:com.lihan.pethome.basic.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 16:30
 * @version:V1.0
 */
@RestController
@RequestMapping("/fastDfs")
public class FastDfsController {

    @PostMapping            //@RequestPart 解析复杂参数
    public AjaxResult upload(@RequestPart(required = true, value = "file") MultipartFile file) throws IOException {
        try {
            //获取文件名
            String originalFilename = file.getOriginalFilename();
            //获取文件名后缀
            String extension = FilenameUtils.getExtension(originalFilename);
            //上传,得到上传后的路径
            String upload = FastDfsApiOpr.upload(file.getBytes(), extension);
            System.out.println(upload);
            return AjaxResult.setResult(upload);
        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.error("上传失败：" + e.getMessage());
        }
    }
    @DeleteMapping
    public AjaxResult delete(String path){
        try {
            String group = path.substring(1);
            System.out.println(group);
            String groupName = group.substring(0,group.indexOf("/"));
            System.out.println(groupName);
            String fileName = group.substring(group.indexOf("/")+1);
            FastDfsApiOpr.delete(groupName,fileName);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("删除失败" + e.getMessage());
        }

    }


}
