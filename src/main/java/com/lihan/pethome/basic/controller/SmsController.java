package com.lihan.pethome.basic.controller;

import com.lihan.pethome.basic.service.ISmsService;
import com.lihan.pethome.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Title: SmsController
 * @Package:com.lihan.pethome.basic.controller
 * @Description:(作用) 发送短语验证
 * @author:lihan
 * @date:2020/8/14 18:58
 * @version:V1.0
 */
@RestController
@RequestMapping("/verificationCode")
public class SmsController {

    @Autowired
    private ISmsService smsService;
    /**
     * @Description:(作用) 注册发送短信验证码
     * @param: [param] 存放了用户的手机号
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/14
     * @version:V1.0
     */
    @PostMapping("/sendPhoneCode")
    private AjaxResult sendPhoneCode(@RequestBody Map<String,String> param){
        return smsService.sendPhoneRegCode(param.get("phone"));
    }

    /**
     * @Description:(作用) 登录发送短信验证码
     * @param: [param] 存放了用户的手机号
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @PostMapping("/sendPhoneLogCode")
    private AjaxResult sendPhoneLogCode(@RequestBody Map<String,String> param){
        return smsService.sendPhoneLogCode(param.get("phone"));
    }

}
