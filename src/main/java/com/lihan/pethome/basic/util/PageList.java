package com.lihan.pethome.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @Title: PageList
 * @Package:com.lihan.pethome.demo.util
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/7 16:12
 * @version:V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageList<T> {

    private List<T> result = new ArrayList<>();
    private Integer total = 0;

}
