package com.lihan.pethome.basic.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * @Title: SendMailUtil
 * @Package:com.lihan.pethome.basic.util
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/12 22:23
 * @version:V1.0
 */
@Component
public class SendMailUtil {

    @Autowired
    private JavaMailSender mailSender;

    public void SendMail(String email,String text) throws MessagingException {
        //创建复杂邮件
        MimeMessage message = mailSender.createMimeMessage();
        //创建复杂邮件的工具对象
        MimeMessageHelper helper = new MimeMessageHelper(message,true,"UTF-8");
        //设置发件人
        helper.setFrom("794243016@qq.com");
        //设置收件人
        helper.setTo(email);
        //设置标题
        helper.setSubject("宠物の家");
        //设置发件内容
        helper.setText(text,true);
        //发送
        mailSender.send(message);
    }

}
