package com.lihan.pethome.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title: AjaxResult
 * @Package:com.lihan.pethome.demo.util
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 12:08
 * @version:V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AjaxResult {

    private Boolean success = true;
    private String msg;

    private Object result;

    public static AjaxResult ok(){
        return new AjaxResult(true,null,null);
    }

    public static AjaxResult error(String msg){
        return new AjaxResult(false,msg,null);
    }

    public static AjaxResult setResult(Object result){
        return new AjaxResult(true, null, result);
    }
}
