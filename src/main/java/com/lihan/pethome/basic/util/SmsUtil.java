package com.lihan.pethome.basic.util;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;

public class SmsUtil {
    //本站用户名
    public static final String UID = "lh794243016";
    //秘钥
    public static final String KEY = "d41d8cd98f00b204e980";

    /**
     * 发送手机验证码
     * @param phone  手机
     * @param text   文本内容
     */
    public static void sendPhoneCode(String phone,String text){
        try {
            //创建一个http请求的客户端
            HttpClient client = new HttpClient();
            //创建post请求的方法
            PostMethod post = new PostMethod("http://utf8.api.smschinese.cn");
            //添加请求头信息
            post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf8");//在头文件中设置转码
            //请求设置的一些参数
            NameValuePair[] data ={ new NameValuePair("Uid", UID),
                    new NameValuePair("Key", KEY),
                    new NameValuePair("smsMob",phone),
                    new NameValuePair("smsText",text)};
            //设置请求体
            post.setRequestBody(data);
            //发送请求
            client.executeMethod(post);
            //获取响应头信息
            Header[] headers = post.getResponseHeaders();
            //获取响应状态
            int statusCode = post.getStatusCode();
            System.out.println("statusCode:"+statusCode);
            for(Header h : headers)
            {
                System.out.println(h.toString());
            }
            String result = new String(post.getResponseBodyAsString().getBytes("utf8"));
            System.out.println(result); //打印返回消息状态
            post.releaseConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
