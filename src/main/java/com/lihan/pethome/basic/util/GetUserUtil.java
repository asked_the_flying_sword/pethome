package com.lihan.pethome.basic.util;

import com.alibaba.fastjson.JSONObject;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.user.entity.User;

import javax.servlet.http.HttpServletRequest;

/**
 * @Title: GetUserUtli
 * @Package:com.lihan.pethome.basic.util
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/23 16:04
 * @version:V1.0
 */
public class GetUserUtil {
    /**
     * @Description:(作用) 获取用户登录信息
     * @param: [request]
     * @return: com.lihan.pethome.user.entity.User
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    public static User getUser(HttpServletRequest request) {
        String header = request.getHeader("U-TOKEN");
        if (header != null) {
            String jsonStr = RedisUtils.INSTANCE.get(header);
            return JSONObject.parseObject(jsonStr, User.class);
        }
        return null;
    }
    /**
     * @Description:(作用)  获取员工登录信息
     * @param: [request]
     * @return: com.lihan.pethome.user.entity.User
     * @author: lihan
     * @date: 2020/8/23
     * @version:V1.0
     */
    public static Employee getEmp(HttpServletRequest request) {
        String header = request.getHeader("E-TOKEN");
        if (header != null) {
            String jsonStr = RedisUtils.INSTANCE.get(header);
            return JSONObject.parseObject(jsonStr, Employee.class);
        }
        return null;
    }

}
