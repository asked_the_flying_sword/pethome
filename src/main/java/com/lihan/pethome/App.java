package com.lihan.pethome;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Title: App
 * @Package:com.lihan.pethome.demo
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 12:02
 * @version:V1.0
 */
@SpringBootApplication
@MapperScan("com.lihan.pethome.*.mapper")
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
