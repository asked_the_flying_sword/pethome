package com.lihan.pethome.quartz.job;

import com.lihan.pethome.order.entity.ProductOrder;
import com.lihan.pethome.order.service.IProductOrderService;
import com.lihan.pethome.pay.constant.PayConstants;
import com.lihan.pethome.pay.entity.PayBill;
import com.lihan.pethome.pay.service.IPayBillService;
import com.lihan.pethome.quartz.entity.QuartzJobInfo;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Map;

/**
 * @Title: MyJob
 * @Package:com.lihan.pethome.quartz.job
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/27 11:48
 * @version:V1.0
 */
public class MyJob implements Job {

    @Autowired
    private IPayBillService payBillService;

    @Autowired
    private IProductOrderService productOrderService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        Object params = jobDataMap.get("params");
        QuartzJobInfo info = (QuartzJobInfo) params;
        System.out.println(info);
        switch (info.getType()) {
            case PayConstants.BUSINESSTYPE_PRODUCT:
                Map<String, Object> params1 = info.getParams();
                //获取订单编号
                String orderSn = (String) params1.get("param");
                PayBill payBill = payBillService.findByOrderSn(orderSn);
                //设置状态为已取消
                payBill.setState(-1);
                //设置修改时间
                payBill.setUpdateTime(new Date());
                //修改支付订单
                payBillService.update(payBill);
                //获取服务订单
                ProductOrder p = productOrderService.findByOrderSn(orderSn);
                //设置状态
                p.setState(-1);
                productOrderService.update(p);
                break;
        }
    }
}
