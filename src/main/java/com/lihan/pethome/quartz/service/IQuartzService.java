package com.lihan.pethome.quartz.service;

import com.lihan.pethome.quartz.entity.QuartzJobInfo;

/**
 * @Title: IQuartzService
 * @Package:com.lihan.pethome.quartz.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/27 11:04
 * @version:V1.0
 */
public interface IQuartzService {

    void addJob(QuartzJobInfo quartzJobInfo);

    void removeJob(String jobName);

}
