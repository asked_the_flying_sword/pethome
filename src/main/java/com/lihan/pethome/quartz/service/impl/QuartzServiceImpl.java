package com.lihan.pethome.quartz.service.impl;

import com.lihan.pethome.quartz.entity.QuartzJobInfo;
import com.lihan.pethome.quartz.job.MyJob;
import com.lihan.pethome.quartz.service.IQuartzService;
import com.lihan.pethome.quartz.util.QuartzUtils;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

/**
 * @Title: QuartzServiceImpl
 * @Package:com.lihan.pethome.quartz.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/27 11:04
 * @version:V1.0
 */
@Service
public class QuartzServiceImpl implements IQuartzService {

    @Autowired
    private SchedulerFactoryBean schedulerFactoryBean;


    @Override
    public void addJob(QuartzJobInfo quartzJobInfo) {
        //获取scheduler对象
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        //添加
        QuartzUtils.addJob(scheduler,quartzJobInfo.getJobName(), MyJob.class,quartzJobInfo,quartzJobInfo.getCronj());
    }

    @Override
    public void removeJob(String jobName) {
        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        //删除定时器
        QuartzUtils.removeJob(scheduler,jobName);
    }
}
