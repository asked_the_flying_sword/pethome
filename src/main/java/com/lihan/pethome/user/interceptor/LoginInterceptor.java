package com.lihan.pethome.user.interceptor;

import com.lihan.pethome.basic.util.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * @Title: LoginInterceptor
 * @Package:com.lihan.pethome.user.interceptor
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/16 17:21
 * @version:V1.0
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取请求头
        String header = request.getHeader("U-TOKEN");
        //判断请求头是否为null
        if (StringUtils.isEmpty(header)) {
            //请求头中的token为null则给前端返回一个JSON对象
            writeJson(response);
            return false;
        }
        //根据请求头中的token取出redis中的userJSON对象
        String user = RedisUtils.INSTANCE.get(header);
        //redis中的数据为空时
        if (Objects.isNull(user)) {
            writeJson(response);
            return false;
        }

        //重新设置userJSON对象存放在redis中的有效期
        RedisUtils.INSTANCE.set(header,user,60*30);
        return true;
    }

    private void writeJson(HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            //设置响应格式
            response.setContentType("text/json;charset=utf-8");
            //获得字符输出流
            writer = response.getWriter();
            //向前端返回JSON字符串
            writer.write("{\"success\":false,\"msg\":\"noUser\"}");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
