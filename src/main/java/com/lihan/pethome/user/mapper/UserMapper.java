package com.lihan.pethome.user.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.user.entity.User;

/**
 * @Title: UserMapper
 * @Package:com.lihan.pethome.user.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14
 * @version:V1.0
 */
public interface UserMapper extends BaseMapper<User> {
    //根据电话查询
    User findUserByTel(String phone);
}
