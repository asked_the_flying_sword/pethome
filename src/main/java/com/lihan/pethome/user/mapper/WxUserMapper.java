package com.lihan.pethome.user.mapper;

import com.lihan.pethome.user.entity.Wxuser;
import org.apache.ibatis.annotations.Param;

/**
 * @Title: WxUserMapper
 * @Package:com.lihan.pethome.user.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/18
 * @version:V1.0
 */
public interface WxUserMapper {
    /**
 * @Description:(作用) 根据openid查询微信用户信息
     * @param: [openid]
     * @return: com.lihan.pethome.user.entity.Wxuser
     * @author: lihan
     * @date: 2020/8/18
     * @version:V1.0
     */
    Wxuser loadWxUserByOpenid(String openid);

    /**
     * @Description:(作用) 保存微信登录的用户信息
     * @param: [user] 微信用户信息
     * @return: void
     * @author: lihan
     * @date: 2020/8/18
     * @version:V1.0
     */
    void save(Wxuser user);

    /**
     * @Description:(作用) 将用户账号和微信账号进行绑定
     * @param: [id, openid]
     * @return: void
     * @author: lihan
     * @date: 2020/8/18
     * @version:V1.0
     */
    void binder(@Param("id") Long id,@Param("openid") String openid);
}
