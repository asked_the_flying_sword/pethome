package com.lihan.pethome.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Wxuser {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String openid;

    /**
     *
     */
    private String nickname;

    /**
     *
     */
    private Integer sex;

    /**
     *
     */
    private String address;

    /**
     *
     */
    private String headimgurl;

    /**
     *
     */
    private String unionid;

    /**
     *
     */
    private User user;
}

