package com.lihan.pethome.user.entity.dto;

import lombok.Data;

/**
 * @Title: UserDto
 * @Package:com.lihan.pethome.user.entity.dto
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/16 10:01
 * @version:V1.0
 */
@Data
public class UserDto {

    private String phone;
    private String code;
    private String password;
    private String confirmPassword;

}
