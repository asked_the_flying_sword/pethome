package com.lihan.pethome.user.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    /**
     *
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 电话
     */
    private String phone;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 密码，md5加密加盐
     */
    private String password;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 创建事件
     */
    private Date createTime;

    /**
     *
     */
    private String headImg;
}

