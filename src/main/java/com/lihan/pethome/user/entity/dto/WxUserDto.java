package com.lihan.pethome.user.entity.dto;

import lombok.Data;

/**
 * @Title: WxUserDto
 * @Package:com.lihan.pethome.user.entity.dto
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/18 17:22
 * @version:V1.0
 */
@Data
public class WxUserDto {

    private String username;
    private String password;
    private String openid;

}
