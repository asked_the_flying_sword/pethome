package com.lihan.pethome.user.constant;

/**
 * 微信常量类
 */
public class WxConstants {
    public static final  String APPID = "wxd853562a0548a7d0";
    public static final  String SECRET = "4a5d5615f93f24bdba2ba8534642dbb6";
    //回调域名
    public static final String REDIRECT_URI = "http://bugtracker.itsource.cn/wechat/callback";

    //获取授权码的地址
    public static final String CODEURL = "https://open.weixin.qq.com/connect/qrconnect?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_login&state=STATE#wechat_redirect";

    //通过code获取token
    public static final String TOKENURL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

    //通过token获取用户信息
    public static final String USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID";
}

