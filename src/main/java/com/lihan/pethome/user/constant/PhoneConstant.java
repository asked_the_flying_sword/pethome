package com.lihan.pethome.user.constant;

/**
 * @Title: PhoneConstant
 * @Package:com.lihan.pethome.user.constant
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14 19:14
 * @version:V1.0
 */
public class PhoneConstant {
    //注册用户的手机验证码的key
    public static final String PHONE_REG = "phone_reg";

    //登录用户的手机验证码的key
    public static final String PHONE_LOG = "phone_log";

}
