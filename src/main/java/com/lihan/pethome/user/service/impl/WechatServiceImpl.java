package com.lihan.pethome.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lihan.pethome.basic.util.HttpClientUtils;
import com.lihan.pethome.basic.util.MD5Utils;
import com.lihan.pethome.basic.util.RedisUtils;
import com.lihan.pethome.user.constant.WxConstants;
import com.lihan.pethome.user.entity.User;
import com.lihan.pethome.user.entity.Wxuser;
import com.lihan.pethome.user.entity.dto.WxUserDto;
import com.lihan.pethome.user.mapper.UserMapper;
import com.lihan.pethome.user.mapper.WxUserMapper;
import com.lihan.pethome.user.service.IUserService;
import com.lihan.pethome.user.service.IWechatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @Title: WechatServiceImpl
 * @Package:com.lihan.pethome.user.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/17 19:11
 * @version:V1.0
 */
@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class WechatServiceImpl implements IWechatService {

    @Autowired
    private WxUserMapper wxUserMapper;

    @Autowired
    private UserMapper userMapper;
    /**
     * @Description:(作用)
     * @param: []
     * @return: java.util.Map<java.lang.String,java.lang.Object>  
     * @author: lihan
     * @date: 2020/8/17
     * @version:V1.0
     */
    @Override
    public Map<String, Object> handleCallback(String code) {
        //获取token对应的url地址
        String tokenUrl = WxConstants.TOKENURL.replace("APPID", WxConstants.APPID)
                .replace("CODE", code)
                .replace("SECRET",WxConstants.SECRET);
        //使用httpClient发送get请求
        String tokenStr = HttpClientUtils.httpGet(tokenUrl);
        //将json字符串转成json对象
        JSONObject jsonObject = JSON.parseObject(tokenStr);
        //得到token
        String access_token = jsonObject.getString("access_token");
        String openid = jsonObject.getString("openid");
        //根据token得到用户信息
        String userInfo = WxConstants.USERINFO_URL.replace("ACCESS_TOKEN", access_token)
                .replace("OPENID", openid);
        //获取用户信息的json字符串
        String userJsonStr = HttpClientUtils.httpGet(userInfo);
        //将用户信息的json字符串转成json对象
        JSONObject userJsonObj = JSON.parseObject(userJsonStr);
        HashMap<String, Object> map = new HashMap<>();
        //用openid查询用户是否存在
        Wxuser wxuser = wxUserMapper.loadWxUserByOpenid(openid);
        if (wxuser == null) {
            Wxuser user = Wxuser.builder().address(userJsonObj.getString("country") + userJsonObj.getString("province") + userJsonObj.getString("city"))
                    .headimgurl(userJsonObj.getString("headimgurl"))
                    .nickname(userJsonObj.getString("nickname"))
                    .openid(openid).sex(userJsonObj.getInteger("sex"))
                    .unionid(userJsonObj.getString("unionid")).build();
            wxUserMapper.save(user);
            //跳转到绑定界面
            map.put("url", "http://localhost:8082/binder.html?openid="+openid);
        }else{
            User user = wxuser.getUser();
            //如果user为null，就表示没有绑定用户
            if (user == null) {
                map.put("url", "http://localhost:8082/binder.html?openid="+openid);
            }else{
                addRedis(map, user);
            }
        }
        return map;
    }
    /**
     * @Description:(作用) 将token和用户信息存入redis中
     * @param: [map, user]
     * @return: void
     * @author: lihan
     * @date: 2020/8/18
     * @version:V1.0
     */
    private void addRedis(HashMap<String, Object> map, User user) {
        //自动登录 存入redis ,作为key
        String token = UUID.randomUUID().toString();
        //将用户信息存入redis中，并设置30分钟有效期
        RedisUtils.INSTANCE.set(token, JSON.toJSONString(user), 30 * 60);
        map.put("token", token);
        map.put("loginUser", user);
    }

    @Override
    @Transactional
    public Map<String,Object> binder(WxUserDto wxUserDto) {
        //根据用户名查询user对象
        User dbUser = userMapper.findUserByTel(wxUserDto.getUsername());
        //创建一个map集合，用于存放token和用户对象
        HashMap<String, Object> map = new HashMap<>();
        //查询的user对象为null,就添加用户
        if (dbUser == null) {
            //使用时间戳作为盐值
            String salt = String.valueOf(System.currentTimeMillis());
            User user = User.builder()
                    .username(wxUserDto.getUsername())
                    .password(MD5Utils.encrypByMd5(wxUserDto.getPassword()+salt))
                    .salt(salt)
                    .state(1).build();
            //保存用户
            userMapper.save(user);
            //将注册的用户和微信账号绑定
            wxUserMapper.binder(user.getId(), wxUserDto.getOpenid());
            //自动登录 存入redis ,作为key
            addRedis(map, user);
        }else{
            if (!dbUser.getPassword().equals(MD5Utils.encrypByMd5(wxUserDto.getPassword() + dbUser.getSalt()))) {
                throw new RuntimeException("密码错误");
            }
            //将注册的用户和微信账号绑定
            wxUserMapper.binder(dbUser.getId(), wxUserDto.getOpenid());
            //自动登录 存入redis ,作为key
            addRedis(map, dbUser);
        }
        return map;
    }
}
