package com.lihan.pethome.user.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.user.entity.User;
import com.lihan.pethome.user.entity.dto.UserDto;

import java.util.Map;

/**
 * @Title: IUserService
 * @Package:com.lihan.pethome.user.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14
 * @version:V1.0
 */
public interface IUserService extends IBaseService<User>{
    //验证手机号是否存在
    AjaxResult checkPhone(Map<String, Object> param);
    //校验短信验证码
    void checkVerificationCode(String phone,String code);
    //手机号注册
    void register(UserDto userDto);
    //账号登录
    User loginUser(String username, String password);
    //手机号登录
    User phoneLogin(String phone, String code);

    void emailRegister(String email, String password, String confirmPassword);
}
