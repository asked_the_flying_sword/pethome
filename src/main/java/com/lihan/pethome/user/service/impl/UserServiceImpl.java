package com.lihan.pethome.user.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.MD5Utils;
import com.lihan.pethome.basic.util.RedisUtils;
import com.lihan.pethome.basic.util.SendMailUtil;
import com.lihan.pethome.user.constant.PhoneConstant;
import com.lihan.pethome.user.entity.User;
import com.lihan.pethome.user.entity.dto.UserDto;
import com.lihan.pethome.user.mapper.UserMapper;
import com.lihan.pethome.user.service.IUserService;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @Title: UserServiceImpl
 * @Package:com.lihan.pethome.user.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14 17:00
 * @version:V1.0
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SendMailUtil sendMailUtil;


    @Override
    public AjaxResult checkPhone(Map<String, Object> param) {
        //获取前台传入的手机号，查询手机号是否存在
        User user = userMapper.findUserByTel((String) param.get("phone"));
        //查询的对象是null时，返回true
        if (Objects.isNull(user)) {
            return AjaxResult.ok();
        }
        return AjaxResult.error("不存在");
    }

    @Override
    public void checkVerificationCode(String phone,String code) {
        //取得前端传的手机号，组装成redis需要的格式
        String key = PhoneConstant.PHONE_REG + ":" + phone;
        //在redis中查找key是否存在
        String val = RedisUtils.INSTANCE.get(key);
        //如果找到了，就表示验证码没失效
        checkRedisValueCode(code, val);
    }


    @Override
    public void register(UserDto userDto) {
        //校验用户
        checkUser(userDto);
        //转换为user
        User user = userDtoToUser(userDto);
        //注册
        userMapper.save(user);
    }
    /**
     * @Description:(作用) 账户登录
     * @param:
     * @return:
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @Override
    public User loginUser(String username, String password) {
        if (Objects.isNull(username)) {
            throw new RuntimeException("请输入用户名");
        }
        //根据用户名查询
        User dbUser = userMapper.findUserByTel(username);
        //判断查询的对象是否为null
        if (Objects.isNull(dbUser)) {
            throw new RuntimeException("用户名不存在");
        }else{
            //判断密码是否则正确，
            if (!dbUser.getPassword().equals(MD5Utils.encrypByMd5(password+dbUser.getSalt()))) {
                throw new RuntimeException("密码错误！");
            }
        }
        return dbUser;
    }

    /**
     * @Description:(作用) 手机号登录，验证短信
     * @param: [phone, code]
     * @return: com.lihan.pethome.user.entity.User
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @Override
    public User phoneLogin(String phone, String code) {
        String value = RedisUtils.INSTANCE.get(PhoneConstant.PHONE_LOG + ":" + phone);
        //如果找到了，就表示验证码没失效
        checkRedisValueCode(code,value);
        //根据手机号在数据库中查询，user不为null，就表示已注册
        User user = userMapper.findUserByTel(phone);
        if (user != null) {
            return user;
        }
        //user等于null，就表示没有注册
        throw new RuntimeException("手机号未注册，请注册");
    }
    /**
     * @Description:(作用) 邮箱注册
     * @param: [email, password, confirmPassword]
     * @return: void
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @Override
    @Transactional
    public void emailRegister(String email, String password, String confirmPassword) {
        try {
            //验证数据
            checkEmailRegInfo(email, password, confirmPassword);
            //先在数据库中查找邮箱是否存在
            User userByTel = userMapper.findUserByTel(email);
            if (Objects.nonNull(userByTel)) {
                throw new RuntimeException("邮箱已注册");
            }
            //使用uuid作为盐值
            String salt = UUID.randomUUID().toString();
            //邮箱没有被注册，就保存到数据库, 默认状态为0-待激活, 使用MD5给密码加密
            User user = User.builder().email(email).state(0).salt(salt).password(MD5Utils.encrypByMd5(password+salt)).username(email).build();
            //保存用户信息
            userMapper.save(user);
            //邮件内容
            String text="<a href=\"http://localhost/user/activationUser/?id="+user.getId()+"\">点击激活</a>";
            //发送邮件
            sendMailUtil.SendMail(email,text);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Description:(作用) 校验邮箱注册的数据
     * @param: [email, password, confirmPassword]
     * @return: void
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    private void checkEmailRegInfo(String email, String password, String confirmPassword) {
        if (StringUtils.isEmpty(email)) {
            throw new RuntimeException("请输入邮箱");
        }
        if (StringUtils.isEmpty(password)) {
            throw new RuntimeException("请输入密码");
        }
        if (StringUtils.isEmpty(confirmPassword)) {
            throw new RuntimeException("请输入确认密码");
        }
        if(!password.equals(confirmPassword)){
            throw new RuntimeException("两次密码不相同");
        }
    }


    /**
     * @Description:(作用) 传入验证码和 存放在redis中的验证码 ，判断验证码是否相同
     * @param: [code, val] 验证码 ，redis中的value
     * @return: void
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    private void checkRedisValueCode(String code,String val) {
        if (StringUtils.isNotBlank(val)) {
            //得到redis中的验证码
            String dbCode = val.split(":")[0];
            //判断验证码是否相同
            if (!dbCode.equalsIgnoreCase(code)) {
                //不相同
                throw new RuntimeException("验证码错误");
            }
        }else {
            //没有在redis中找到，表示验证码已失效，提示用户重新发送
            throw new RuntimeException("验证码错误，请重新发送");
        }
    }


    /**
     * @Description:(作用) userDto转换为user对象
     * @param: [userDto]
     * @return: void
     * @author: lihan
     * @date: 2020/8/16
     * @version:V1.0
     */
    private User userDtoToUser(UserDto userDto) {
        User user = new User();
        //将userDto中的数据复制到user对象中
        BeanUtils.copyProperties(userDto,user);
        //将手机号设为默认用户名
        user.setUsername(userDto.getPhone());
        //因为是手机号注册，所有默认为激活状态
        user.setState(1);
        //使用UUID随机生成的数设为盐值
        String salt = UUID.randomUUID().toString();
        user.setSalt(salt);
        //将密码使用md5加密
        user.setPassword(MD5Utils.encrypByMd5(user.getPassword()+salt));
        return user;
    }
    /**
     * @Description:(作用)    校验前端的数据
     * @param: [userDto]
     * @return: void
     * @author: lihan
     * @date: 2020/8/16
     * @version:V1.0
     */
    private void checkUser(UserDto userDto) {
        System.out.println(userDto);
        if (StringUtils.isEmpty(userDto.getPhone())) {
            throw new RuntimeException("手机号不能为空");
        }
        if (StringUtils.isEmpty(userDto.getPassword())) {
            throw new RuntimeException("密码不能为空");
        }
        if (StringUtils.isEmpty(userDto.getCode())) {
            throw new RuntimeException("请输入验证码");
        }
        if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            throw new RuntimeException("两次密码不相同");
        }
        //校验验证码
        checkVerificationCode(userDto.getPhone(), userDto.getCode());

    }
}
