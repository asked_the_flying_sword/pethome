package com.lihan.pethome.user.service;

import com.lihan.pethome.user.entity.dto.WxUserDto;

import java.util.Map;

/**
 * @Title: WechatService
 * @Package:com.lihan.pethome.user.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/17
 * @version:V1.0
 */
public interface IWechatService {

    Map<String,Object> handleCallback(String code);

    Map<String,Object> binder(WxUserDto wxUserDto);
}
