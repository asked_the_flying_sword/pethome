package com.lihan.pethome.user.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.user.constant.WxConstants;
import com.lihan.pethome.user.entity.dto.WxUserDto;
import com.lihan.pethome.user.service.IWechatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Title: WechatController
 * @Package:com.lihan.pethome.user.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/17 15:12
 * @version:V1.0
 */
@Controller
@RequestMapping("/wechat")
public class WechatController {

    @Autowired
    private IWechatService wechatService;


    @RequestMapping("/toLogin")
    public String toLogin() {
        String code = WxConstants.CODEURL.replace("APPID", WxConstants.APPID)
                .replace("REDIRECT_URI", WxConstants.REDIRECT_URI);
        return "redirect:"+code;
    }

    @RequestMapping("/callback")
    public String callback(String code) {
        return "redirect:http://localhost:8082/callback.html?code="+code;
    }

    /**
     * @Description:(作用) 根据授权码得到token
     * @param: [param]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/17
     * @version:V1.0
     */
    @RequestMapping("/accessToken")
    @ResponseBody
    public AjaxResult accessToken(@RequestBody Map<String,String> param) {
        try {
            Map<String, Object> map = wechatService.handleCallback(param.get("code"));
            return AjaxResult.setResult(map);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * @Description:(作用) 绑定用户信息
     * @param: [wxUserDto] 绑定用户信息的临时对象
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/18
     * @version:V1.0
     */
    @RequestMapping("/binder")
    @ResponseBody
    public AjaxResult binder(@RequestBody WxUserDto wxUserDto) {
        try {
            Map<String,Object> msp = wechatService.binder(wxUserDto);
            return AjaxResult.setResult(msp);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }

    }

}
