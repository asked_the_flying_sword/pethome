package com.lihan.pethome.user.controller;

import com.alibaba.fastjson.JSON;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.RedisUtils;
import com.lihan.pethome.user.entity.User;
import com.lihan.pethome.user.entity.dto.UserDto;
import com.lihan.pethome.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @Title: UserController
 * @Package:com.lihan.pethome.user.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/14 16:27
 * @version:V1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;
    /**
     * @Description:(作用) 校验手机号是否存在
     * @param: [param]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/15
     * @version:V1.0
     */
    @PostMapping("/checkPhone")
    public AjaxResult checkPhone(@RequestBody Map<String,Object> param){
        return userService.checkPhone(param);
    }
    /**
     * @Description:(作用) 校验短信验证码是否正确
     * @param: [code]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/15
     * @version:V1.0
     */
    @PostMapping("/checkVerificationCode")
    public AjaxResult checkVerificationCode(@RequestBody Map<String,String> param){
        try {
            userService.checkVerificationCode(param.get("phone"),param.get("code"));
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

    /**
     * @Description:(作用) 用户注册
     * @param: [userDto]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/16
     * @version:V1.0
     */
    @PutMapping("/register")
    public AjaxResult register(@RequestBody UserDto userDto){
        try {
            userService.register(userDto);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * @Description:(作用) 账号登录
     * @param: [user] 用户对象
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody User user) {
        try {
            //验证用户名和密码,验证成功返回user对象
            User dbUser = userService.loginUser(user.getUsername(),user.getPassword());
            return AjaxResult.setResult(addUserToRedis(dbUser));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * @Description:(作用) 手机号登录
     * @param: [userDto]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @PostMapping("/phoneLogin")
    public AjaxResult phoneLogin(@RequestBody UserDto userDto) {
        try {
            User user = null;
            HashMap<String, Object> map = null;
            if (userDto != null) {
                user = userService.phoneLogin(userDto.getPhone(), userDto.getCode());
                map = addUserToRedis(user);
            }
            return AjaxResult.setResult(map);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * @Description:(作用) 将用户对象和token保存到redis中
     * @param: [user] 用户信息对象
     * @return: java.util.HashMap<java.lang.String,java.lang.Object>
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    private HashMap<String, Object> addUserToRedis(User user) {
        user.setPassword(null);
        String jsonStr = JSON.toJSONString(user);
        //生成令牌，作为存放redis中数据的key
        String token = UUID.randomUUID().toString();
        //将用户的信息放进redis中 ,并设置有效期为三十分钟
        RedisUtils.INSTANCE.set(token,jsonStr,60*30);
        //将token和用户信息，封装成map对象，并返回给前端
        HashMap<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("loginUser", user);
        return map;
    }

    /**
     * @Description:(作用) 邮箱注册
     * @param: [map]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/21
     * @version:V1.0
     */
    @PostMapping("/emailRegister")
    public AjaxResult emailRegister(@RequestBody HashMap<String, String> map) {
        try {
            if (Objects.nonNull(map)) {
                userService.emailRegister(map.get("email"),map.get("password"),map.get("confirmPassword"));
            }else{
                throw new RuntimeException("网络超时，请稍后再试");
            }
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * @Description:(作用) 邮箱注册 集合账号
     * @param: [id]
     * @return: java.lang.String
     * @author: lihan
     * @date: 2020/8/22
     * @version:V1.0
     */
    @GetMapping("/activationUser")
    public String activationUser(Long id) {
        userService.update(User.builder().id(id).state(1).build());
        return "激活成功";
    }

}
