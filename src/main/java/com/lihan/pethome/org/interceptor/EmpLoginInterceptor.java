package com.lihan.pethome.org.interceptor;

import com.lihan.pethome.basic.util.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * @Title: EmpLoginInterceptor
 * @Package:com.lihan.pethome.org.interceptor
 * @Description:(作用) 员工登录拦截
 * @author:lihan
 * @date:2020/8/16 22:01
 * @version:V1.0
 */
@Component
public class EmpLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //取得令牌
        String header = request.getHeader("E-TOKEN");
        if (StringUtils.isEmpty(header)) {
            writeJson(response);
            return false;
        }
        String empJsonStr = RedisUtils.INSTANCE.get(header);
        if (StringUtils.isEmpty(empJsonStr)) {
            writeJson(response);
            return false;
        }else{
            //当在redis中查询的数据不等于空时，重置有效时间
            RedisUtils.INSTANCE.set(header,empJsonStr,60*30);
        }

        return true;
    }

    /**
     * @Description:(作用) 当token等于null和在redis中查询的数据为null时，返回的JSON数据
     * @param: [response]
     * @return: void
     * @author: lihan
     * @date: 2020/8/16
     * @version:V1.0
     */
    private void writeJson(HttpServletResponse response) {
        PrintWriter writer = null;
        try {
            response.setContentType("text/json;charset=utf-8");
            writer = response.getWriter();
            writer.write("{\"success\":false,\"msg\":\"noEmp\"}");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (Objects.nonNull(writer)) {
                writer.close();
            }
        }
    }
}
