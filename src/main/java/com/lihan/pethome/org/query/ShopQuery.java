package com.lihan.pethome.org.query;

import com.lihan.pethome.basic.query.BaseQuery;
import lombok.Data;

/**
 * @Title: ShopQuery
 * @Package:com.lihan.pethome.org.query
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/12 17:28
 * @version:V1.0
 */
@Data
public class ShopQuery extends BaseQuery {
    private String name;
    private String tel;
    private String address;
    private Integer state;
}
