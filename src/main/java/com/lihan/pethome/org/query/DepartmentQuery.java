package com.lihan.pethome.org.query;

import com.lihan.pethome.basic.query.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Title: DepartmentQuery
 * @Package:com.lihan.pethome.demo.query
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/7 16:09
 * @version:V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DepartmentQuery extends BaseQuery {

    private String name;
    private Integer state;

}
