package com.lihan.pethome.org.query;

import com.lihan.pethome.basic.query.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Title: EmployeeQuery
 * @Package:com.lihan.pethome.demo.query
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/8 21:22
 * @version:V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EmployeeQuery extends BaseQuery {

    private String username;
    private String email;
    private String phone;
    private Integer state;


}
