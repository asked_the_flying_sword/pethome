package com.lihan.pethome.org.controller;

import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.query.EmployeeQuery;
import com.lihan.pethome.org.service.IEmployeeService;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Title: EmployeeController
 * @Package:com.lihan.pethome.demo.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/8 17:38
 * @version:V1.0
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    /**
     * @Description:(作用) 根据条件查询所有员工
     * @param: [query] 查询条件
     * @return: com.lihan.pethome.basic.util.PageList<com.lihan.pethome.org.entity.Employee>
     * @author: lihan
     * @date: 2020/8/8
     * @version:V1.0
     */
    @PatchMapping
    public PageList<Employee> findAllEmployee(@RequestBody EmployeeQuery query) {
        System.out.println(query);
        return employeeService.findAll(query);
    }

    /**
     * @Description:(作用)  修改员工信息
     * @param: [employee] 员工对象 ： 前台传输的是json格式
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/9
     * @version:V1.0
     */
    @PostMapping
    public AjaxResult updateEmployee(@RequestBody Employee employee){
        try {
            //调用修改方法
            employeeService.update(employee);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("修改失败:" + e.getMessage());
        }
    }

    /**
     * @Description:(作用)  添加员工信息
     * @param: [employee] 员工对象 ： 前台传输的是json格式
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/9
     * @version:V1.0
     */
    @PutMapping
    public AjaxResult addEmployee(@RequestBody Employee employee){
        try {
            //调用修改方法
            employeeService.save(employee);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("添加失败:" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 批量删除
     * @param: [ids]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/9
     * @version:V1.0
     */
    @DeleteMapping("{ids}")
    public AjaxResult delEmployee(@PathVariable List<Long> ids){
        try {
            employeeService.del(ids);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("删除失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用)   激活账号
     * @param: []
     * @return: void
     * @author: lihan
     * @date: 2020/8/12
     * @version:V1.0
     */
    @GetMapping("/activationEmployee")
    public String activationEmployee(Long id){
        employeeService.updateById(id);
        return "激活成功";
    }

}
