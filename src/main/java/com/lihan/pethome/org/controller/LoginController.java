package com.lihan.pethome.org.controller;

import com.alibaba.fastjson.JSON;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.RedisUtils;
import com.lihan.pethome.basic.util.StrUtils;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.UUID;

/**
 * @Title: LoginController
 * @Package:com.lihan.pethome.org.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 18:35
 * @version:V1.0
 */
@RestController
public class LoginController {

    @Autowired
    private IEmployeeService employeeService;

    @PostMapping("/login")
    public AjaxResult login(@RequestBody Employee employee){
        try {
            //登录验证
            Employee emp = employeeService.login(employee);
            //将emp对象转为json字符串
            String jsonString = JSON.toJSONString(emp);
            //使用工具类生成一个10位的随机字符串作为令牌
            String token = StrUtils.getComplexRandomString(10);
            //将token作为key, emp转成的json字符串作为value 存入redis, 并设置30分钟的有效期
            RedisUtils.INSTANCE.set(token,jsonString,60*30);
            HashMap<String, Object> map = new HashMap<>();
            map.put("token", token);
            map.put("loginEmp", emp);
            return AjaxResult.setResult(map);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

}
