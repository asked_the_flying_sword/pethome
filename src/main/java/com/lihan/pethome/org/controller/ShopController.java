package com.lihan.pethome.org.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.org.query.ShopQuery;
import com.lihan.pethome.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title: ShopController
 * @Package:com.lihan.pethome.org.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 13:58
 * @version:V1.0
 */
@RestController
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    private IShopService shopService;
    /**
     * @Description:(作用) 添加店铺
     * @param: [shop] 店铺对象
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/12
     * @version:V1.0
     */
    @PostMapping("/settledIn")
    public AjaxResult settledIn(@RequestBody Shop shop){
        try {
            shopService.settledIn(shop);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("失败");
        }
    }
    /**
     * @Description:(作用) 根据员工id查询店铺状态
     * @param: [id]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author: lihan
     * @date: 2020/8/12
     * @version:V1.0
     */
    @PostMapping("/findState/{id}")
    public Shop findShopState(@PathVariable Long id){
        //根据id查询得到店铺对象
        Shop shop = shopService.findShopState(id);
        return shop;
    }
    /**
     * @Description:(作用) 高级查询和分页
     * @param: [shopQuery]
     * @return: com.lihan.pethome.basic.util.PageList<com.lihan.pethome.org.entity.Shop>
     * @author: lihan
     * @date: 2020/8/12
     * @version:V1.0
     */
    @PatchMapping
    public PageList<Shop> loadShops(@RequestBody ShopQuery shopQuery){
        return shopService.findAll(shopQuery);
    }

    /**
     * @Description:(作用) 修改店铺的状态
     * @param: [shop]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    @PostMapping("/save")
    public AjaxResult save(@RequestBody Shop shop) {
        try {
            if(shop.getState() == 1){
                shopService.stateSuccess(shop);
            }else {
                shopService.stateFail(shop);
            }
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("失败" + e.getMessage());
        }

    }
    /**
     * @Description:(作用)  根据员工id查询对应商品的审核信息
     * @param: [id]
     * @return: java.util.List<com.lihan.pethome.org.entity.Shop>
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    @PostMapping("/showStateInfo/{id}")
    public List<Shop> showStateInfo(@PathVariable Long id){
        return shopService.showStateInfo(id);
    }

    @PostMapping("/updateShop")
    public AjaxResult updateShop(@RequestBody Shop shop){
        try {
            shopService.update(shop);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("失败");
        }
    }

}
