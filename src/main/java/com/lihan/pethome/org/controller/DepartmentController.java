package com.lihan.pethome.org.controller;

import com.lihan.pethome.org.entity.Department;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.query.DepartmentQuery;
import com.lihan.pethome.org.service.IDepartmentService;
import com.lihan.pethome.org.service.IEmployeeService;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Title: DepartmentController
 * @Package:com.lihan.pethome.demo.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 12:07
 * @version:V1.0
 */
@RestController
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    @Autowired
    private IEmployeeService employeeService;
    /**
     * @Description:(作用) 添加部门
     * @param: [department]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PutMapping
    public AjaxResult save(@RequestBody Department department){
        try {
            //调用service的添加方法
            departmentService.save(department);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("添加失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 修改部门
     * @param: [department]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PostMapping
    public AjaxResult update(@RequestBody Department department){
        try {
            //调用service的修改方法
            departmentService.update(department);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("修改失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 根据id删除部门
     * @param: [id]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PatchMapping("/del")
    public AjaxResult del(@RequestBody List<Long> ids){
        System.err.println(ids);
        try {
            //调用service的根据id删除方法
            departmentService.del(ids);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("删除失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 根据id查询单个部门
     * @param: [id]
     * @return: com.lihan.pethome.org.entity.Department
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @GetMapping("{id}")
    public Department findById(@PathVariable("id") Long id){
        return departmentService.findById(id);
    }
    /**
     * @Description:(作用) 查询所有部门
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.Department>
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PatchMapping
    public PageList<Department> findAll(@RequestBody DepartmentQuery departmentQuery){
        return departmentService.findAll(departmentQuery);
    }
    /**
     * @Description:(作用) 查询所有部门信息
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.Department>
     * @author: lihan
     * @date: 2020/8/8
     * @version:V1.0
     */
    @PatchMapping("/loadAll")
    public List<Department> loadAll(){
        return departmentService.list();
    }
    @PatchMapping("/loadAllEmployee")
    public List<Employee> loadAllEmployee(){
        return employeeService.list();
    }

}
