package com.lihan.pethome.org.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.org.entity.Shop;

import java.util.List;

/**
 * @Title: ShopMapper
 * @Package:com.lihan.pethome.org.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 11:06
 * @version:V1.0
 */
public interface ShopMapper extends BaseMapper<Shop> {
    void updateShopState(Shop shop);

    void addFailInfo(Shop shop);
    /*
     * @Description:(作用) 根据员工id查询店铺
     * @param: [id]
     * @return: com.lihan.pethome.org.entity.Shop
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */

    List<Shop> findInfoByEmpId(Long id);
    /*
     * @Description:(作用) 根据员工id查询店铺信息
     * @param: [id] 员工id
     * @return: com.lihan.pethome.org.entity.Shop
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    Shop findShopByEmpId(Long id);
}
