package com.lihan.pethome.org.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.org.entity.Employee;
import org.apache.ibatis.annotations.Param;

/**
 * @Title: EmployeeMapper
 * @Package:com.lihan.pethome.demo.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/8
 * @version:V1.0
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

    Employee findByName(String username);
    //修改员工状态
    void updateById(@Param("id") Long id ,@Param("state") Integer state);
}
