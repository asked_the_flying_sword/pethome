package com.lihan.pethome.org.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.org.entity.Department;

/**
 * @Title: DepartmentMapper
 * @Package:com.lihan.pethome.demo.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 11:39
 * @version:V1.0
 */
public interface DepartmentMapper extends BaseMapper<Department> {

}
