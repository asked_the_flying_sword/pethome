package com.lihan.pethome.org.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.org.entity.Employee;

/**
 * @Title: IEmployeeService
 * @Package:com.lihan.pethome.demo.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/8
 * @version:V1.0
 */
public interface IEmployeeService extends IBaseService<Employee> {
    //员工登录验证
    Employee login(Employee employee);
    //根据id修改状态
    void updateById(Long id);
}
