package com.lihan.pethome.org.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.org.entity.Shop;

import java.util.List;
import java.util.Map;

/**
 * @Title: IShopService
 * @Package:com.lihan.pethome.org.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 11:07
 * @version:V1.0
 */
public interface IShopService extends IBaseService<Shop> {

    void settledIn(Shop shop);

    void stateSuccess(Shop shop);

    void stateFail(Shop shop);

    List<Shop> showStateInfo(Long id);
    /**
     * @Description:(作用) 根据员工id查询店铺信息
     * @param: [id]
     * @return: com.lihan.pethome.org.entity.Shop
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    Shop findShopState(Long id);
}
