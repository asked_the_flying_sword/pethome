package com.lihan.pethome.org.service.impl;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.basic.util.SendMailUtil;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.org.mapper.EmployeeMapper;
import com.lihan.pethome.org.mapper.ShopMapper;
import com.lihan.pethome.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Map;

/**
 * @Title: ShopServiceImpl
 * @Package:com.lihan.pethome.org.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 13:56
 * @version:V1.0
 */
@Service
public class ShopServiceImpl extends BaseServiceImpl<Shop> implements IShopService {

    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private SendMailUtil sendMailUtil;

    @Override
    @Transactional
    public void settledIn(Shop shop) {
        //获取员工对象
        Employee admin = shop.getAdmin();
        //设置员工的状态 0待审核
        admin.setState(0);
        //保存员工信息，得到员工id
        employeeMapper.save(admin);
        //将有id的员工存到shop对象
        shop.setAdmin(admin);
        //保存店铺对象
        shopMapper.save(shop);
    }
    /**
     * @Description:(作用) 审核通过
     * @param: [shop]
     * @return: void
     * @author: lihan
     * @date: 2020/8/12
     * @version:V1.0
     */
    @Override
    @Transactional
    public void stateSuccess(Shop shop) {
        System.out.println(shop.getAdmin().getId());
        try {
            //修改店铺的状态
            shopMapper.updateShopState(shop);
            //得到店铺信息
            Employee dbEmp = employeeMapper.findById(shop.getAdmin().getId());
            String email = dbEmp.getEmail();
            String text="<a href=\"http://localhost/employee/activationEmployee/?id="+dbEmp.getId()+"\">点击激活</a>";

            //发送激活邮件
            sendMailUtil.SendMail(email,text);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
    /**
     * @Description:(作用) 审核失败
     * @param: [shop]
     * @return: void
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    @Override
    @Transactional
    public void stateFail(Shop shop) {
        //修改店铺的状态
        shopMapper.updateShopState(shop);
        //保存审核失败的原因
        shopMapper.addFailInfo(shop);
        //根据id获取员工的信息
        Employee dbEmp = employeeMapper.findById(shop.getId());
        //修改员工的状态
        employeeMapper.updateById(dbEmp.getId(),shop.getState());
    }
    /**
     * @Description:(作用) 查询审核失败的原因
     * @param: [id]
     * @return: java.util.Map<java.lang.String,java.lang.Object>
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    @Override
    public List<Shop> showStateInfo(Long id) {
        //查询审核失败的原因，封装到shop对象
        return shopMapper.findInfoByEmpId(id);
    }
    /**
     * @Description:(作用) 查询店铺审核信息
     * @param: [id] 员工id
     * @return: com.lihan.pethome.org.entity.Shop
     * @author: lihan
     * @date: 2020/8/13
     * @version:V1.0
     */
    @Override
    public Shop findShopState(Long id) {

        return shopMapper.findShopByEmpId(id);
    }
}
