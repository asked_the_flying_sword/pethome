package com.lihan.pethome.org.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.org.entity.Department;
import com.lihan.pethome.org.mapper.DepartmentMapper;
import com.lihan.pethome.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: DepartmentServiceImpl
 * @Package:com.lihan.pethome.demo.service.impl
 * @Description:(作用)I
 * @author:lihan
 * @date:2020/8/4 11:59
 * @version:V1.0
 */
@Service
public class DepartmentServiceImpl extends BaseServiceImpl<Department> implements IDepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

}
