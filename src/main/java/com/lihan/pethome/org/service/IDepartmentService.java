package com.lihan.pethome.org.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.org.entity.Department;

/**
 * @Title: DepartmentService
 * @Package:com.lihan.pethome.demo.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4
 * @version:V1.0
 */
public interface IDepartmentService extends IBaseService<Department> {

}
