package com.lihan.pethome.org.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.mapper.DepartmentMapper;
import com.lihan.pethome.org.mapper.EmployeeMapper;
import com.lihan.pethome.org.query.EmployeeQuery;
import com.lihan.pethome.org.service.IEmployeeService;
import com.lihan.pethome.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @Title: IEmployeeServiceImpl
 * @Package:com.lihan.pethome.demo.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/8 17:16
 * @version:V1.0
 */
@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Employee login(Employee employee) {
        //根据用户名查询
        Employee dbEmployee = employeeMapper.findByName(employee.getUsername());
        //查询的用户为null时
        if(Objects.isNull(employee)){
            throw new RuntimeException("用户名不存在");
        }else{
            //判断密码是否正确
            if (!employee.getPassword().equals(dbEmployee.getPassword())) {
                throw new RuntimeException("密码错误");
            }else{
                return dbEmployee;
            }
        }

    }
    /**
     * @Description:(作用) 根据id修改状态
     * @param: [id] 员工id
     * @return: void
     * @author: lihan
     * @date: 2020/8/12
     * @version:V1.0
     */
    @Override
    public void updateById(Long id) {
        employeeMapper.updateById(id,1);
    }
}
