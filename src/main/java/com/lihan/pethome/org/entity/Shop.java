package com.lihan.pethome.org.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Title: Shop
 * @Package:com.lihan.pethome.org.entity
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/11 11:03
 * @version:V1.0
 */
@Data
public class Shop {
    private Long id;
    //店铺名
    private String name;
    //手机号
    private String tel;
    //创建时间
    private Date registerTime = new Date();
    //审核状态 0待审核 1审核通过 -1审核未通过
    private int state = 0;
    //店铺地址
    private String address;
    //店铺logo
    private String logo;
    //管理员
    private Employee admin;

    //审核不通过原因
    private String info;

}
