package com.lihan.pethome.org.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title: Department
 * @Package:com.lihan.pethome.demo.entity
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 11:05
 * @version:V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Department {
    private Long id;
    private String sn;
    //部门名称
    private String name;
    //状态
    private Integer state;
    //经理id
    private Long managerId;
    //上级部门id
    private Long parentId;
    //    部门经理
    private Employee employee;
    //    上级部门
    private Department parent;

}
