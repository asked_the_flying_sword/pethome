package com.lihan.pethome.org.entity;

import lombok.Data;

@Data
public class Employee {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String username;

    /**
     *
     */
    private String email;

    /**
     *
     */
    private String phone;

    /**
     *
     */
    private String password;

    /**
     *
     */
    private Integer age;

    /**
     *
     */
    private Integer state;
}

