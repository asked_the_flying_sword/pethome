package com.lihan.pethome.order.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.order.entity.Product;
import com.lihan.pethome.order.entity.UserAddress;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Title: DepartmentMapper
 * @Package:com.lihan.pethome.demo.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 11:39
 * @version:V1.0
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
