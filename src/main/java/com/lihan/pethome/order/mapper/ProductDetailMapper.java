package com.lihan.pethome.order.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.order.entity.ProductDetail;

/**
 * @Title: ProductDetailMapper
 * @Package:com.lihan.pethome.order.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/19 11:06
 * @version:V1.0
 */
public interface ProductDetailMapper extends BaseMapper<ProductDetail> {
}
