package com.lihan.pethome.order.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.order.entity.Product;
import com.lihan.pethome.org.entity.Department;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Title: DepartmentMapper
 * @Package:com.lihan.pethome.demo.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 11:39
 * @version:V1.0
 */
public interface ProductMapper extends BaseMapper<Product> {

    void updateSateOn(@Param("ids") List<Long> ids,@Param("data") Date date);

    void updateSateOff(@Param("ids") List<Long> ids,@Param("data") Date date);
}
