package com.lihan.pethome.order.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.order.entity.PetAcquisitionOrder;

/**
 * @Title: PetAcquisitionOrderMapper
 * @Package:com.lihan.pethome.order.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25
 * @version:V1.0
 */
public interface PetAcquisitionOrderMapper extends BaseMapper<PetAcquisitionOrder> {
    void updateById(Long id);
}
