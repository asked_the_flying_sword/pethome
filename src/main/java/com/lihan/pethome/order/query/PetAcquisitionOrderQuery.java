package com.lihan.pethome.order.query;

import com.lihan.pethome.basic.query.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Title: PetAcquisitionOrderQuery
 * @Package:com.lihan.pethome.order.query
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25 14:42
 * @version:V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PetAcquisitionOrderQuery extends BaseQuery {

    private String digest;
    private Integer state;

}
