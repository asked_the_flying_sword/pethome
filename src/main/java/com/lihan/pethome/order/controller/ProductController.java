package com.lihan.pethome.order.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.order.entity.Product;
import com.lihan.pethome.order.query.ProductQuery;
import com.lihan.pethome.order.service.IProductService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Title: productController
 * @Package:com.lihan.pethome.demo.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4 12:07
 * @version:V1.0
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private IProductService productService;

    /**
     * @Description:(作用) 添加商品
     * @param: [product]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PutMapping
    public AjaxResult save(@RequestBody Product product){
        try {
            //调用service的添加方法
            productService.save(product);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("添加失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 修改商品
     * @param: [product]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PostMapping
    public AjaxResult update(@RequestBody Product product){
        try {
            //调用service的修改方法
            productService.update(product);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("修改失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 根据id删除商品
     * @param: [id]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PatchMapping("/del")
    public AjaxResult del(@RequestBody List<Long> ids){
        System.err.println(ids);
        try {
            //调用service的根据id删除方法
            productService.del(ids);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("删除失败：" + e.getMessage());
        }
    }
    /**
     * @Description:(作用) 根据id查询单个商品
     * @param: [id]
     * @return: com.lihan.pethome.org.entity.Product
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @GetMapping("{id}")
    public Product findById(@PathVariable("id") Long id){
        return productService.findById(id);
    }
    /**
     * @Description:(作用) 查询所有商品
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.Product>
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @PatchMapping
    public PageList<Product> findAll(@RequestBody ProductQuery productQuery){
        return productService.findAll(productQuery);
    }

    /**
     * @Description:(作用) 上架
     * @param: [ids]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/19
     * @version:V1.0
     */
    @PatchMapping("/updateSateOn")
    public AjaxResult updateSateOn(@RequestBody List<Long> ids){
        try {
            //调用service的根据id修改方法
            productService.updateSateOn(ids);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
    /**
     * @Description:(作用) 下架
     * @param: [ids]
     * @return: com.lihan.pethome.basic.util.AjaxResult
     * @author: lihan
     * @date: 2020/8/19
     * @version:V1.0
     */
    @PatchMapping("/updateSateOff")
    public AjaxResult updateSateOff(@RequestBody List<Long> ids){
        try {
            //调用service的根据id修改方法
            productService.updateSateOff(ids);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }
//=====================================前台信息============================================
    /**
     * @Description:(作用) 查询所有商品信息
     * @param: []
     * @return: java.util.List<com.lihan.pethome.org.entity.Product>
     * @author: lihan
     * @date: 2020/8/8
     * @version:V1.0
     */
    @PatchMapping("/loadAll")
    public PageList<Product> loadAll(@RequestBody ProductQuery productQuery){
        return productService.findAll(productQuery);
    }
    /**
     * @Description:(作用) 查询单个服务
     * @param: [id]
     * @return: com.lihan.pethome.order.entity.Product
     * @author: lihan
     * @date: 2020/8/19
     * @version:V1.0
     */
    @PostMapping("/findById/{id}")
    public Product toProductInfo(@PathVariable Long id) {
        return productService.findById(id);
    }

}
