package com.lihan.pethome.order.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.PageList;
import com.lihan.pethome.order.entity.PetAcquisitionOrder;
import com.lihan.pethome.order.query.PetAcquisitionOrderQuery;
import com.lihan.pethome.order.service.IPetAcquisitionOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Title: PetAcquisitionOrderController
 * @Package:com.lihan.pethome.order.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25 14:39
 * @version:V1.0
 */
@RestController
@RequestMapping("/petAcquisition")
public class PetAcquisitionOrderController {

    @Autowired
    private IPetAcquisitionOrderService petAcquisitionOrderService;

    /**
     * @Description:(作用) 根据条件查询所有宠物收购订单
     * @param: [query]
     * @return: com.lihan.pethome.basic.util.PageList<com.lihan.pethome.order.entity.PetAcquisitionOrder>
     * @author: lihan
     * @date: 2020/8/25
     * @version:V1.0
     */
    @PatchMapping
    public PageList<PetAcquisitionOrder> getPetAcquisitionOrders(@RequestBody PetAcquisitionOrderQuery query) {
        return petAcquisitionOrderService.findAll(query);
    }

    @PostMapping("{id}")
    public AjaxResult updatePetAcquisitionOrderState(@PathVariable Long id) {
        try {
            petAcquisitionOrderService.updateById(id);
            return AjaxResult.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

}
