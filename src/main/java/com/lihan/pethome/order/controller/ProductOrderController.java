package com.lihan.pethome.order.controller;

import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.basic.util.GetUserUtil;
import com.lihan.pethome.order.service.IProductOrderService;
import com.lihan.pethome.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Title: ProductOrderController
 * @Package:com.lihan.pethome.order.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25 15:40
 * @version:V1.0
 */
@RestController
@RequestMapping("/productOrder")
public class ProductOrderController {

    //服务订单对应的service
    @Autowired
    private IProductOrderService productOrderService;

    @PostMapping("/submitOrder")
    public AjaxResult submitOrder(@RequestBody Map<String,Object> param, HttpServletRequest request) {
        try {
            //获取当前登录的用户对象
            User user = GetUserUtil.getUser(request);
            //得到支付宝返回的前端代码
            String result = productOrderService.submitOrder(param, user);
            return AjaxResult.setResult(result);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

}
