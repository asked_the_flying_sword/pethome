package com.lihan.pethome.order.entity;

import com.lihan.pethome.org.entity.Employee;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.pet.entity.Pet;
import com.lihan.pethome.user.entity.User;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 宠物收购订单
 */
@Data
public class PetAcquisitionOrder{
    private Long id;
    //摘要
    private String digest;
    //0 待支付(余额支付) 1 待报账(垫付) 2 待打款(银行转账)  3 完成
    private Integer state;
    //收购价格
    private BigDecimal price;
    //收购地址
	private String address;
	//订单编号
    private String orderSn;
    //宠物
    private Pet pet;
    //信息发布者
    private User user;
    //支付类型  0垫付 1余额 2银行转账
    private Integer paytype;
    //收购订单对应的店铺
    private Shop shop;
    //员工
    private Employee employee;

}
