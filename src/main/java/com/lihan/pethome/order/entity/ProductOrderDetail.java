package com.lihan.pethome.order.entity;


import lombok.Data;

import java.util.Date;

@Data
public class ProductOrderDetail {
    private Long id;
    //创建时间
    private Date createTime = new Date();
    //服务产品
    private Product product;
    //销售数量
    private Integer saleCount;
    //服务订单
    private ProductOrder order;


}
