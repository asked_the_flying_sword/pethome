package com.lihan.pethome.order.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    /**
     *
     */
    private Long id;

    /**
     * 服务名称
     */
    private String name;

    /**
     * 存放fastdfs地址，多个资源使用，分割
     */
    private String resources;

    /**
     *
     */
    private BigDecimal saleprice;

    /**
     *
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss",timezone = "GMT+8")
    private Date offsaletime;

    /**
     *
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss",timezone = "GMT+8")
    private Date onsaletime;

    /**
     * 状态：0下架 1上架
     */
    private Long state = 0L;

    /**
     *
     */
    private String costprice;

    /**
     *
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss",timezone = "GMT+8")
    private Date createtime = new Date();

    /**
     *
     */
    private Long salecount;

    private ProductDetail productDetail;
}

