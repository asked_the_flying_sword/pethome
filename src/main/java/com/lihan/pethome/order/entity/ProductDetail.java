package com.lihan.pethome.order.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDetail {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private Long productId;

    /**
     * 简介
     */
    private String intro;

    /**
     * 预约须知
     */
    private String orderNotice;
}

