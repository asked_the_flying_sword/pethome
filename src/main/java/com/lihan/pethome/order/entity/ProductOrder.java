package com.lihan.pethome.order.entity;


import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.user.entity.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class ProductOrder{
    private Long id;
    //摘要
    private String digest;
    //状态   待支付0 待消费1 待确认2 完成3 取消-1
    private Integer state;
    //价格
    private BigDecimal price;
    //支付方式
    private Integer payType;
    //订单编号
    private String orderSn;
    //支付编号
    private String paySn;
    //最后支付时间
    private Date lastPayTime;
    //最后确认时间
    private Date lastConfirmTime;
    //用户
    private User user;
    //店铺
    private Shop shop;



}
