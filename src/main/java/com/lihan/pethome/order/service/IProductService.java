package com.lihan.pethome.order.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.order.entity.Product;
import com.lihan.pethome.org.entity.Department;

import java.util.List;

/**
 * @Title: DepartmentService
 * @Package:com.lihan.pethome.demo.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/4
 * @version:V1.0
 */
public interface IProductService extends IBaseService<Product> {

    /**
     * @Description:(作用) 添加商品
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    void save(Product product);

    /**
     * @Description:(作用) 添加商品
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    void update(Product product);

    /**
     * @Description:(作用) 上架
     * @param: [ids]
     * @return: void
     * @author: lihan
     * @date: 2020/8/19
     * @version:V1.0
     */
    void updateSateOn(List<Long> ids);

    void updateSateOff(List<Long> ids);
}
