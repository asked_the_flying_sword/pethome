package com.lihan.pethome.order.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.order.entity.ProductOrder;
import com.lihan.pethome.user.entity.User;

import java.util.Map;

/**
 * @Title: IProductOrderService
 * @Package:com.lihan.pethome.order.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25
 * @version:V1.0
 */
public interface IProductOrderService extends IBaseService<ProductOrder> {


    String submitOrder(Map<String, Object> param, User user);

    ProductOrder findByOrderSn(String orderSn);
}
