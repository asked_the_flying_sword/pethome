package com.lihan.pethome.order.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.order.entity.UserAddress;
import com.lihan.pethome.order.service.IUserAddressService;
import org.springframework.stereotype.Service;

/**
 * @Title: UserAddressServiceImpl
 * @Package:com.lihan.pethome.order.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25 16:16
 * @version:V1.0
 */
@Service
public class UserAddressServiceImpl extends BaseServiceImpl<UserAddress> implements IUserAddressService {
}
