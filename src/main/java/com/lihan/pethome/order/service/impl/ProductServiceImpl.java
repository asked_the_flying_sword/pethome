package com.lihan.pethome.order.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.order.entity.Product;
import com.lihan.pethome.order.entity.ProductDetail;
import com.lihan.pethome.order.mapper.ProductDetailMapper;
import com.lihan.pethome.order.mapper.ProductMapper;
import com.lihan.pethome.order.service.IProductService;
import com.lihan.pethome.org.entity.Department;
import com.lihan.pethome.org.mapper.DepartmentMapper;
import com.lihan.pethome.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Title: DepartmentServiceImpl
 * @Package:com.lihan.pethome.demo.service.impl
 * @Description:(作用)I
 * @author:lihan
 * @date:2020/8/4 11:59
 * @version:V1.0
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductDetailMapper productDetailMapper;

    /**
     * @Description:(作用) 根据数据id删除
     * @param: [id] 数据id
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void del(List<Long> ids) {
        //先删除productDetail中关联的数据
        productDetailMapper.delete(ids);
        //再删除product信息
        productMapper.delete(ids);
    }

    /**
     * @Description:(作用) 添加数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void save(Product product) {
        //先保存产品信息得到产品的id
        productMapper.save(product);

        //得到产品详细信息的对象
        ProductDetail productDetail = product.getProductDetail();
        //将得到的product的id给 productDetail对象
        productDetail.setProductId(product.getId());
        //保存数据
        productDetailMapper.save(productDetail);
    }
    /**
     * @Description:(作用) 修改数据
     * @param: [t] 数据对象
     * @return: void
     * @author: lihan
     * @date: 2020/8/4
     * @version:V1.0
     */
    @Override
    @Transactional
    public void update(Product product) {
        //获得商品详细信息的对象
        ProductDetail productDetail = product.getProductDetail();
        productDetail.setProductId(product.getId());
        //修改商品的详细信息
        productDetailMapper.update(productDetail);
        //修改商品信息
        productMapper.update(product);
    }

    /**
     * @Description:(作用) 上架商品
     * @param: [ids]
     * @return: void
     * @author: lihan
     * @date: 2020/8/19
     * @version:V1.0
     */
    @Override
    @Transactional
    public void updateSateOn(List<Long> ids) {
        Date date = new Date();
        productMapper.updateSateOn(ids,date);
    }

    @Override
    @Transactional
    public void updateSateOff(List<Long> ids) {
        Date date = new Date();
        productMapper.updateSateOff(ids,date);
    }

}
