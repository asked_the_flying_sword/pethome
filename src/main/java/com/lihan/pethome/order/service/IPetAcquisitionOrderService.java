package com.lihan.pethome.order.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.order.entity.PetAcquisitionOrder;

/**
 * @Title: IPetAcquisitionOrderService
 * @Package:com.lihan.pethome.order.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25
 * @version:V1.0
 */
public interface IPetAcquisitionOrderService extends IBaseService<PetAcquisitionOrder> {

    void updateById(Long id);
}
