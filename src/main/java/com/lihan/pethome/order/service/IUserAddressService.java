package com.lihan.pethome.order.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.order.entity.UserAddress;

/**
 * @Title: IUserAddressService
 * @Package:com.lihan.pethome.order.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25
 * @version:V1.0
 */
public interface IUserAddressService extends IBaseService<UserAddress> {
}
