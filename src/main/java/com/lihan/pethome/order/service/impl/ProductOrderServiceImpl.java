package com.lihan.pethome.order.service.impl;

import com.lihan.pethome.basic.entity.Point;
import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.basic.util.CodeGenerateUtils;
import com.lihan.pethome.basic.util.DistanceUtil;
import com.lihan.pethome.order.entity.*;
import com.lihan.pethome.order.mapper.*;
import com.lihan.pethome.order.service.IProductOrderService;
import com.lihan.pethome.order.service.IUserAddressService;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.org.mapper.ShopMapper;
import com.lihan.pethome.org.service.IShopService;
import com.lihan.pethome.pay.constant.PayConstants;
import com.lihan.pethome.pay.entity.PayBill;
import com.lihan.pethome.pay.service.IPayBillService;
import com.lihan.pethome.quartz.entity.QuartzJobInfo;
import com.lihan.pethome.quartz.service.IQuartzService;
import com.lihan.pethome.user.entity.User;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Title: ProductOrderServiceImpl
 * @Package:com.lihan.pethome.order.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25 15:58
 * @version:V1.0
 */
@Service
public class ProductOrderServiceImpl extends BaseServiceImpl<ProductOrder> implements IProductOrderService {

    //用户地址mapper
    @Autowired
    private UserAddressMapper userAddressMapper;

    //店铺mapper
    @Autowired
    private ShopMapper shopMapper;

    //服务mapper
    @Autowired
    private ProductMapper productMapper;
    //服务订单mapper
    @Autowired
    private ProductOrderMapper productOrderMapper;
    //服务订单明细mapper
    @Autowired
    private ProductOrderDetailMapper productOrderDetailMapper;
    //订单地址mapper
    @Autowired
    private OrderAddressMapper orderAddressMapper;
    //支付订单service
    @Autowired
    private IPayBillService payBillService;

    @Autowired
    private IQuartzService quartzService;

    @Override
    @Transactional
    public String submitOrder(Map<String, Object> param, User user) {
        //1、获取用户地址的id
        Integer addressId = (Integer) param.get("addressId");
        //2、获取用户支付方式
        Integer payType = (Integer) param.get("payType");
        //3、获取产品服务信息 {服务id，服务数量}
        List<Map<String,Object>> products = (List<Map<String, Object>>) param.get("products");
        //4、根据地址id查询用户地址对象
        UserAddress userAddress = userAddressMapper.findById(Long.valueOf(addressId));
        //5、获取用户的详细地址
        String fullAddress = userAddress.getFullAddress();
        //6、根据用户的详细地址获取经纬度
        Point point = DistanceUtil.getPoint(fullAddress);
        //7、根据经纬度得到距离用户地址最近的店铺对象
        Shop shop = DistanceUtil.getNearestShop(point, shopMapper.list());
        //8、创建服务订单 （封装成方法）
        ProductOrder productOrder = createProductOrder(products, shop, user, payType);
        //9、保存服务订单
        productOrderMapper.save(productOrder);
        //10、创建服务订单明细 （封装成方法）
        List<ProductOrderDetail> productOrderDetail = createProductOrderDetail(products, productOrder);
        //11、保存服务订单明细
        productOrderDetailMapper.batchSave(productOrderDetail);
        //12、将用户地址转成订单地址
        OrderAddress orderAddress = userAddressToOrderAddress(userAddress);
        //13、保存订单地址
        orderAddressMapper.save(orderAddress);
        //创建支付账单
        PayBill payBill = createPayBill(productOrder,payType);
        //保存支付账单
        payBillService.save(payBill);
        //跳转到支付界面
        String formHtml = payBillService.toPay(payBill);
        //创建定时器任务
        QuartzJobInfo info = createQuartzJobInfo(payBill);
        quartzService.addJob(info);
        return formHtml;
    }

    @Override
    public ProductOrder findByOrderSn(String orderSn) {
        return productOrderMapper.findByOrderSn(orderSn);
    }

    /**
     * @Description:(作用) 创建定时器任务
     * @param: []
     * @return: com.lihan.pethome.quartz.entity.QuartzJobInfo
     * @author: lihan
     * @date: 2020/8/27
     * @version:V1.0
     */
    private QuartzJobInfo createQuartzJobInfo(PayBill payBill) {
        QuartzJobInfo info = new QuartzJobInfo();
        //设置类型
        info.setType(payBill.getBusinessType());
        //设置有效时间
        info.setFireDate(payBill.getLastPayTime());
        info.setJobName(payBill.getBusinessType()+payBill.getId());
        HashMap<String, Object> map = new HashMap<>();
        map.put("param", payBill.getOrderSn());
        info.setParams(map);
        return info;
    }

    /**
     * @Description:(作用) 创建支付账单
     * @param: [productOrder : 服务订单]
     * @param: payType : 支付方式
     * @return: com.lihan.pethome.pay.entity.PayBill
     * @author: lihan
     * @date: 2020/8/26
     * @version:V1.0
     */
    private PayBill createPayBill(ProductOrder productOrder, Integer payType) {
        PayBill payBill = new PayBill();
        //设置账单摘要
        payBill.setDigest(productOrder.getDigest());
        //设置账单金额
        payBill.setMoney(productOrder.getPrice());
        //设置状态为待支付 0
        payBill.setState(0);
        //设置最后支付时间
        payBill.setLastPayTime(productOrder.getLastPayTime());
        //设置支付方式
        payBill.setPayChannel(payType);
        //设置业务类型
        payBill.setBusinessType(PayConstants.BUSINESSTYPE_PRODUCT);
        //设置业务键
        payBill.setBusinessKey(productOrder.getId());
        //设置用户
        payBill.setUser(productOrder.getUser());
        //设置店铺
        payBill.setShop(productOrder.getShop());
        //设置订单编号
        payBill.setOrderSn(productOrder.getOrderSn());
        return payBill;
    }

    /**
     * @Description:(作用) 将用户地址转成订单地址
     * @param: [userAddress] 用户地址信息
     * @return: com.lihan.pethome.order.entity.OrderAddress
     * @author: lihan
     * @date: 2020/8/25
     * @version:V1.0
     */
    private OrderAddress userAddressToOrderAddress(UserAddress userAddress) {
        OrderAddress orderAddress = new OrderAddress();
        //使用工具类将用户地址中的字段，复制到订单地址中
        BeanUtils.copyProperties(userAddress,orderAddress);
        //设置订单地址的创建时间
        orderAddress.setCreateTime(new Date());
        return orderAddress;
    }

    /**
     * @Description:(作用) 创建产品服务订单明细
     * @param: [products, productOrder]
     * @return: com.lihan.pethome.order.entity.ProductOrderDetail
     * @author: lihan
     * @date: 2020/8/25
     * @version:V1.0
     */
    private List<ProductOrderDetail> createProductOrderDetail(List<Map<String, Object>> products, ProductOrder productOrder) {
        //创建一个集合来存放服务订单明细
        List<ProductOrderDetail> details = new ArrayList<>();
        for (Map<String, Object> product : products) {
            //得到产品服务的id
            Integer id = (Integer) product.get("id");
            //得到产品服务的数量
            Integer num = (Integer) product.get("num");
            //根据id查询服务对象
            Product dbProduct = productMapper.findById(Long.valueOf(id));
            //创建服务订单明细对象
            ProductOrderDetail detail = new ProductOrderDetail();
            //设置服务订单
            detail.setOrder(productOrder);
            //设置服务产品
            detail.setProduct(dbProduct);
            //设置服务数量
            detail.setSaleCount(num);
            //添加服务订单明细
            details.add(detail);
        }
        return details;
    }

    /**
     * @Description:(作用) 创建服务订单
     * @param: products 用户购买的服务
     * @param: shop     提供服务的商家
     * @param: user     用户
     * @param: payType  支付方式
     * @return: com.lihan.pethome.order.entity.ProductOrder
     * @author: lihan
     * @date: 2020/8/25
     * @version:V1.0
     */
    private ProductOrder createProductOrder(List<Map<String, Object>> products, Shop shop, User user, Integer payType) {
        ProductOrder productOrder = new ProductOrder();
        //设置订单标题
        productOrder.setDigest("[服务订单]"+shop.getName()+"进行服务");
        //设置订单状态为待支付   状态   待支付0 待消费1 待确认2 完成3 取消-1
        productOrder.setState(0);
        //计算服务的总价
        BigDecimal totalPrice = new BigDecimal(0);
        for (Map<String, Object> product : products) {
            //得到产品服务的id
            Integer id = (Integer) product.get("id");
            //得到产品服务的数量
            Integer num = (Integer) product.get("num");
            //根据id查询服务对象
            Product dbProduct = productMapper.findById(Long.valueOf(id));
            //得到服务的售价
            BigDecimal saleprice = dbProduct.getSaleprice();
            //计算总价
            totalPrice = totalPrice.add(saleprice.multiply(new BigDecimal(num)));
        }
        //设置服务订单的金额
        productOrder.setPrice(totalPrice);
        //设置支付方式
        productOrder.setPayType(payType);
        //设置订单编号   根据用户id生成订单编号
        productOrder.setOrderSn(CodeGenerateUtils.generateOrderSn(user.getId()));
        //设置支付编号   使用CodeGenerateUtils工具类生成支付编号
        productOrder.setPaySn(CodeGenerateUtils.generateUnionPaySn());
        //设置最后支付时间  未支付订单的有效期 为 15分钟
        productOrder.setLastPayTime(DateUtils.addMinutes(new Date(),15));
        //设置购买的用户
        productOrder.setUser(user);
        //设置提供服务的店铺
        productOrder.setShop(shop);
        return productOrder;
    }
}
