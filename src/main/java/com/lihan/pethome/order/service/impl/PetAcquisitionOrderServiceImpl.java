package com.lihan.pethome.order.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.order.entity.PetAcquisitionOrder;
import com.lihan.pethome.order.mapper.PetAcquisitionOrderMapper;
import com.lihan.pethome.order.service.IPetAcquisitionOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Title: PetAcquisitionOrderServiceImpl
 * @Package:com.lihan.pethome.order.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/25 14:45
 * @version:V1.0
 */
@Service
public class PetAcquisitionOrderServiceImpl extends BaseServiceImpl<PetAcquisitionOrder> implements IPetAcquisitionOrderService {

    @Autowired
    private PetAcquisitionOrderMapper petAcquisitionOrderMapper;

    @Override
    @Transactional
    public void updateById(Long id) {
        petAcquisitionOrderMapper.updateById(id);
    }
}
