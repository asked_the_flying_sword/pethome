package com.lihan.pethome.pay.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.pay.entity.AlipayInfo;

/**
 * @Title: IAlipayInfoService
 * @Package:com.lihan.pethome.pay.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26 17:48
 * @version:V1.0
 */
public interface IAlipayInfoService extends IBaseService<AlipayInfo> {
    //根据订单编号查询
    AlipayInfo findByOrderSn(String out_trade_no);
}
