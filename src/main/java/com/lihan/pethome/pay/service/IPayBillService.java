package com.lihan.pethome.pay.service;

import com.lihan.pethome.basic.service.IBaseService;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.pay.entity.PayBill;

/**
 * @Title: IPayBillService
 * @Package:com.lihan.pethome.pay.service
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26 16:14
 * @version:V1.0
 */
public interface IPayBillService extends IBaseService<PayBill> {

    String toPay(PayBill payBill);

    String handleOrder(String out_trade_no, String trade_no);

    PayBill findByOrderSn(String orderSn);
}
