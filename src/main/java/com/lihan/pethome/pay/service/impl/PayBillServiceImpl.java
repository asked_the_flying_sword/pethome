package com.lihan.pethome.pay.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.order.entity.ProductOrder;
import com.lihan.pethome.order.mapper.ProductOrderMapper;
import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.pay.config.AlipayConfig;
import com.lihan.pethome.pay.constant.PayConstants;
import com.lihan.pethome.pay.entity.AlipayInfo;
import com.lihan.pethome.pay.entity.PayBill;
import com.lihan.pethome.pay.mapper.AlipayInfoMapper;
import com.lihan.pethome.pay.mapper.PayBillMapper;
import com.lihan.pethome.pay.service.IPayBillService;
import com.lihan.pethome.quartz.service.IQuartzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Title: PayBillServiceImpl
 * @Package:com.lihan.pethome.pay.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26 16:15
 * @version:V1.0
 */
@Service
public class PayBillServiceImpl extends BaseServiceImpl<PayBill> implements IPayBillService {

    //支付宝密钥mapper
    @Autowired
    private AlipayInfoMapper alipayInfoMapper;

    //支付订单mapper
    @Autowired
    private PayBillMapper payBillMapper;
    //服务订单mapper
    @Autowired
    private ProductOrderMapper productOrderMapper;
    //定时器service
    @Autowired
    private IQuartzService quartzService;

    @Override
    public String toPay(PayBill payBill) {
        try {
            //获取密钥
            AlipayInfo alipayInfo = alipayInfoMapper.findById(payBill.getShop().getId());

            //获得初始化的AlipayClient
            AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, alipayInfo.getAppid(),
                    alipayInfo.getMerchant_private_key(), "json", AlipayConfig.charset,
                    alipayInfo.getAlipay_public_key(), AlipayConfig.sign_type);

            //设置请求参数
            AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
            alipayRequest.setReturnUrl(AlipayConfig.return_url);
            alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

            //商户订单号，商户网站订单系统中唯一订单号，必填
            String out_trade_no = payBill.getOrderSn();
            //付款金额，必填
            String total_amount = payBill.getMoney().toString();
            //订单名称，必填
            String subject = "我秦始皇，打钱";
            //商品描述，可空
            String body = payBill.getDigest();

            alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                    + "\"total_amount\":\""+ total_amount +"\","
                    + "\"subject\":\""+ subject +"\","
                    + "\"body\":\""+ body +"\","
                    + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

            //请求
            return alipayClient.pageExecute(alipayRequest).getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @Description:(作用) 处理请求
     * @param: [out_trade_no, trade_no]
     * @return: void
     * @author: lihan
     * @date: 2020/8/26
     * @version:V1.0
     */
    @Override
    public String handleOrder(String out_trade_no, String trade_no) {
        //根据订单编号获取支付订单
        PayBill payBill = payBillMapper.findByOrderSn(out_trade_no);
        if (payBill.getState() != 0) {
            return "fail";
        }
        //修改支付订单
        //设置修改时间
        payBill.setUpdateTime(new Date());
        //设置支付订单状态
        payBill.setState(1);
        //保存支付订单
        payBillMapper.update(payBill);
        String jobName = "";
        //获取服务订单
        switch (payBill.getBusinessType()) {
            case PayConstants.BUSINESSTYPE_PRODUCT:
                ProductOrder productOrder = productOrderMapper.findById(payBill.getBusinessKey());
                //待消费
                productOrder.setState(1);
                //修改服务订单
                productOrderMapper.update(productOrder);
                jobName = PayConstants.BUSINESSTYPE_PRODUCT+productOrder.getId();
                break;
            case PayConstants.BUSINESSTYPE_ADOPT:
                break;
            case PayConstants.BUSINESSTYPE_FIND_MASTER:
                break;
            case PayConstants.BUSINESSTYPE_GOODS:
                break;
        }
        quartzService.removeJob(jobName);
        return "success";
    }

    @Override
    public PayBill findByOrderSn(String orderSn) {
        return payBillMapper.findByOrderSn(orderSn);
    }
}
