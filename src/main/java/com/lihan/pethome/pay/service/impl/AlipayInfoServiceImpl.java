package com.lihan.pethome.pay.service.impl;

import com.lihan.pethome.basic.service.impl.BaseServiceImpl;
import com.lihan.pethome.pay.entity.AlipayInfo;
import com.lihan.pethome.pay.mapper.AlipayInfoMapper;
import com.lihan.pethome.pay.service.IAlipayInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Title: AlipayInfoServiceImpl
 * @Package:com.lihan.pethome.pay.service.impl
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26 17:49
 * @version:V1.0
 */
@Service
public class AlipayInfoServiceImpl extends BaseServiceImpl<AlipayInfo> implements IAlipayInfoService {

    @Autowired
    private AlipayInfoMapper alipayInfoMapper;

    @Override
    public AlipayInfo findByOrderSn(String out_trade_no) {
        return alipayInfoMapper.findByOrderSn(out_trade_no);
    }
}
