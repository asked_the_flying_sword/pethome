package com.lihan.pethome.pay.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.pay.entity.PayBill;

/**
 * @Title: PayBillMapper
 * @Package:com.lihan.pethome.pay.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26
 * @version:V1.0
 */
public interface PayBillMapper extends BaseMapper<PayBill> {

    PayBill findByOrderSn(String out_trade_no);
}
