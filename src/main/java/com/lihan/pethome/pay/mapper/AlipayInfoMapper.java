package com.lihan.pethome.pay.mapper;

import com.lihan.pethome.basic.mapper.BaseMapper;
import com.lihan.pethome.pay.entity.AlipayInfo;

/**
 * @Title: AlipayInfoMapper
 * @Package:com.lihan.pethome.pay.mapper
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26
 * @version:V1.0
 */
public interface AlipayInfoMapper extends BaseMapper<AlipayInfo> {

    AlipayInfo findByOrderSn(String out_trade_no);
}
