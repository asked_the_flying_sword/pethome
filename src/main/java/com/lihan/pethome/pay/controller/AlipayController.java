package com.lihan.pethome.pay.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.lihan.pethome.basic.util.AjaxResult;
import com.lihan.pethome.pay.config.AlipayConfig;
import com.lihan.pethome.pay.constant.PayConstants;
import com.lihan.pethome.pay.entity.AlipayInfo;
import com.lihan.pethome.pay.service.IAlipayInfoService;
import com.lihan.pethome.pay.service.IPayBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.alipay.api.AlipayConstants.SIGN_TYPE;
import static redis.clients.jedis.Protocol.CHARSET;

/**
 * @Title: AlipayController
 * @Package:com.lihan.pethome.pay.controller
 * @Description:(作用)
 * @author:lihan
 * @date:2020/8/26 17:16
 * @version:V1.0
 */
@RestController
@RequestMapping("/ali")
public class AlipayController {

    @Autowired
    private IAlipayInfoService alipayInfoService;

    @Autowired
    private IPayBillService payBillService;

    @PostMapping("/notify")
    public String notifyRequest(HttpServletRequest request) {
        try {
            Map<String,String> params = new HashMap<>();
            //把支付宝传递的参数，封装到map集合中
            Map<String,String[]> requestParams = request.getParameterMap();
            for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext();) {
                String name = iter.next();
                String[] values = requestParams.get(name);
                String valueStr = "";
                for (int i = 0; i < values.length; i++) {
                    valueStr = (i == values.length - 1) ? valueStr + values[i]
                            : valueStr + values[i] + ",";
                }
                params.put(name, valueStr);
            }
            //获取订单编号
            String out_trade_no = params.get("out_trade_no");
            //根据订单编号获取公钥
            AlipayInfo alipayInfo = alipayInfoService.findByOrderSn(out_trade_no);
            boolean signVerified = AlipaySignature.rsaCheckV1(params, alipayInfo.getAlipay_public_key(), AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名
            if(signVerified) {//验证成功
                //支付宝交易号
                String trade_no = new String(request.getParameter("trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
                //交易状态
                String trade_status = request.getParameter("trade_status");
                //交易成功或者结束
                if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
                    //处理订单状态(服务订单，支付订单)
                    payBillService.handleOrder(out_trade_no, trade_no);
                    //做我们自己额外的业务逻辑
                    return "success";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fail";
    }

}
