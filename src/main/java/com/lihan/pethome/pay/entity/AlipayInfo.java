package com.lihan.pethome.pay.entity;

import com.lihan.pethome.org.entity.Shop;
import lombok.Data;

/**
 * 支付信息
 */
@Data
public class AlipayInfo {
    private Long id;
    //应用私钥
    private String merchant_private_key;
    //appid
    private String appid;
    //支付宝公钥
    private String alipay_public_key;
    //店铺
    private Shop shop;

}
