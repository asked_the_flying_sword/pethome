package com.lihan.pethome.pay.entity;

import com.lihan.pethome.org.entity.Shop;
import com.lihan.pethome.user.entity.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付单
 */
@Data
public class PayBill  {
    private Long id;
    //摘要
    private String digest;
    //金额
    private BigDecimal money;
    //支付单编号
    private String unionPaySn;
    //支付状态    0 待支付      1已支付    -1 取消
    private Integer state;
    //最后支付时间
    private Date lastPayTime;
    //0 余额 1 支付宝 2 微信 3 银联
    private Integer payChannel;
    //业务类型
    private String businessType;
    //订单编号
    private String orderSn;
    //业务键
    private Long businessKey;
    //修改时间(记录支付宝返回状态的时间)
    private Date updateTime;
    //创建时间
    private Date createTime = new Date();
    //用户
    private User user;
    //店铺
    private Shop shop;

}
