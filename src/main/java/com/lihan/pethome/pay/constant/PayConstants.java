package com.lihan.pethome.pay.constant;

public class PayConstants {
    public static  final String BUSINESSTYPE_ADOPT = "businessType_adopt";
    public static  final String BUSINESSTYPE_FIND_MASTER = "businesstype_find_master";
    public static  final String BUSINESSTYPE_GOODS = "businesstype_goods";
    public static  final String BUSINESSTYPE_PRODUCT = "businesstype_product";
}
